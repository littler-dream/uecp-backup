package edu.zhku.uecp.service;

import com.alibaba.druid.sql.dialect.oracle.ast.stmt.OracleGotoStatement;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import edu.zhku.uecp.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@Service
public class NotService {
    @Autowired
    private NoticesRepository notRepo;

    /**
     * 功能：分页，排序
     *
     * @param pageNum  页码
     * @param pageSize 一页的大小
     * @return
     */
    public PageRequest byPage(Integer pageNum, Integer pageSize) {
        PageRequest pageable;
        //返回第一页(每页pageSize个)查询结果，查询结果按优先级升序排列
        List<Sort.Order> orders = new ArrayList<>();
        Sort sort;

        orders.clear();
        orders.add(new Sort.Order(Sort.Direction.ASC, "priority"));
        sort = Sort.by(orders);
        pageable = PageRequest.of(pageNum, pageSize, sort);
        return pageable;
    }

    public Map<String, Object> findAllNotsNoPage() {
        Map<String, Object> orgs = new HashMap<>(3);
        orgs.put("nots", notRepo.findAll());
        return orgs;
    }

    public Map<String, Object> findAllNots(Integer pageNum, Integer pageSize) {
        Map<String, Object> orgs = new HashMap<>(3);
        PageRequest pageable;
        pageable = byPage(pageNum, pageSize);
        orgs.put("nots", notRepo.findAll(pageable));
        return orgs;
    }

    public Notices findById(Integer id) {
        return notRepo.findById(id).get();
    }

    //存在为真
    public boolean isExist(Integer notID) {
        return notRepo.existsById(notID);
    }

    //删除公告
    public void delNot(Integer notId) {
        notRepo.deleteById(notId);
    }

    //添加公告
    public void addNot(Notices not2) {
        Notices not = new Notices();
        not.setPriority(not2.getPriority());
        not.setPicture(not2.getPicture());
        not.setPicture_name(not2.getPicture_name());
        not.setContent(not2.getContent());
        notRepo.save(not);
    }

    //修改公告
    public String upNot(Integer notid, Notices not2) {
        String befPicture;
        Notices not = new Notices();
        not.setId(notid);
        not.setPriority(not2.getPriority());
        not.setPicture(not2.getPicture());
        not.setPicture_name(not2.getPicture_name());
        not.setContent(not2.getContent());
        befPicture = notRepo.findNoticesById(notid).getPicture();
        notRepo.save(not);
        return befPicture;
    }

    //模糊查询公告通过关键字(分页)
    public Map<String, Object> searchNotBykeyword(String keyword, Integer pageNum, Integer pageSize) {
        Map<String, Object> orgs = new HashMap<>(3);
        PageRequest pageable;
        pageable = byPage(pageNum, pageSize);
        orgs.put("nots", notRepo.findNoticesByName(keyword, pageable));
        return orgs;
    }

    //模糊查询公告通过关键字(不分页)
    public Map<String, Object> searchNotBykeywordNoPage(String keyword) {
        Map<String, Object> orgs = new HashMap<>(3);
        PageRequest pageable;
        orgs.put("nots", notRepo.findNoticesByName(keyword));
        return orgs;
    }
}

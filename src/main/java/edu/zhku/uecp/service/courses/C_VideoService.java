package edu.zhku.uecp.service.courses;

import edu.zhku.uecp.model.CourseRepository;
import edu.zhku.uecp.model.Course_Video;
import edu.zhku.uecp.model.Course_VideoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.Map;

@Service
@Transactional//事务管理注解
public class C_VideoService {
    @Autowired
    private CourseRepository courseRepo;
    @Autowired
    private Course_VideoRepository c_VideoRepo;


    /*随机获取最高x位数的数字(不超16位)*/
    public int getNumber(Integer x){
        String random = Math.random()+"";
        String suijishu = random.substring(3, 3+x);
        int result = Integer.parseInt(suijishu);
        return result;
    }

    /*判断是否存在*/
    public boolean isExist(Integer id){
        return c_VideoRepo.existsById(id);
    }

    /*添加单个视频信息*/
    public String addVideo(Course_Video c_video)/* throws SQLException */{
        int id = getNumber(9);
        c_video.setId(id);//设置随机数主键
        c_VideoRepo.save(c_video);//保存视频信息
        id = c_video.getCourseId();//将id转为当前课程id
        courseRepo.updateVideoNum(id);//更新当前课程的视频的计数
        String vName = c_video.getName();
        return vName;
    }

    /*删除单个视频信息*/
    public void deleteVideo(Integer id){
        Course_Video c_video = c_VideoRepo.getOne(id);
        id = c_video.getCourseId();//获取当前课程id
        c_VideoRepo.delete(c_video);//删除掉这个视频
        courseRepo.updateVideoNum(id);//对应课程的视频计数-1
    }

    /*修改单个视频信息*/
    public String updateVideo(Integer id, Course_Video c_video){
        c_VideoRepo.save(c_video);//保存新的课程信息
        String vName = c_video.getName();
        return vName;
    }

    /*根据课程id查找视频信息*/
    public Map<String ,Object> findAllByCourseId(Integer id){
        Map<String ,Object> c_videos = new HashMap<>(3);
        c_videos.put("videos",c_VideoRepo.findVideoByCourseId(id));
        return c_videos;
    }
}
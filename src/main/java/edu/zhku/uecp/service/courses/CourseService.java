package edu.zhku.uecp.service.courses;

import edu.zhku.uecp.model.Course;
import edu.zhku.uecp.model.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CourseService {

    @Autowired
    private CourseRepository courseRepo;

    /**
     * 功能：分页，排序
     * @param pageNum 页码（从0计）
     * @param pageSize 一页的大小
     * @return
     */
    public PageRequest byPage(Integer pageNum, Integer pageSize){
        PageRequest pageable;
        //返回第一页(每页pageSize个)查询结果，查询结果按名字升序排列
        List<Sort.Order> orders = new ArrayList<>();
        Sort sort;
        orders.clear();
        orders.add(new Sort.Order(Sort.Direction.ASC, "name"));
        sort = Sort.by(orders);
        pageable = PageRequest.of(pageNum, pageSize, sort);
        return pageable;
    }

    /*功能同上，排序上以评分为标准，升序*/
    public PageRequest byPage1(Integer pageNum, Integer pageSize){
        PageRequest pageable;
        //返回第一页(每页pageSize个)查询结果，查询结果按名字升序排列
        List<Sort.Order> orders = new ArrayList<>();
        Sort sort;
        orders.clear();
        orders.add(new Sort.Order(Sort.Direction.ASC, "score"));
        sort = Sort.by(orders);
        pageable = PageRequest.of(pageNum, pageSize, sort);
        return pageable;
    }

    /*随机获取最高x位数的数字(不超16位)*/
    public int getNumber(Integer x){
        String random = Math.random()+"";
        String suijishu = random.substring(3, 3+x);
        int result = Integer.parseInt(suijishu);
        return result;
    }

    /*判断是否存在*/
    public boolean isExist(Integer courseId){
        return courseRepo.existsById(courseId);
    }

    /*不分页获取所有课程*/
    public Map<String,Object> findAllCourseNoPage(){
        Map<String, Object> courses = new HashMap<>(3);
        courses.put("courses", courseRepo.findAll());
        return courses;
    }

    /*分页获取所有课程*/
    public  Map<String,Object> findAllCourseByPage(Integer pageNum,Integer pageSize){
        Map<String, Object> courses = new HashMap<>(3);
        PageRequest pageable;
        pageable = byPage(pageNum,pageSize);
        courses.put("courses", courseRepo.findAll(pageable));
        return courses;
    }

    /*添加单个课程信息*/
    public Integer saveOneCourseMsg(Course course){
        int id = getNumber(8);
        course.setId(id);
        courseRepo.save(course);
        return id;
    }

    /*修改课程信息*/
    public String updateCourseMsg(Integer id, Course course){
        String cName;
        courseRepo.save(course);
        cName = courseRepo.findCourseById(id).getName();
        return cName;
    }

    /*根据名称模糊查询，分页*/
    public Map<String, Object> searchCourseByName(String name,Integer pageNum,Integer pageSize){
        Map<String, Object> courses = new HashMap<>(3);
        name = "%"+name+"%";
        PageRequest pageable;
        pageable = byPage(pageNum,pageSize);
        courses.put("courses",courseRepo.findCourseMsgByName(name,pageable));
        return courses;
    }

    /*根据id删除课程信息*/
    public boolean deleteCourse(Integer id){
        courseRepo.deleteById(id);
        if(isExist(id)){
            return false;
        }
        return true;
    }

    /*获取所有课程等级*/
    public List<String> getAllCourseLevel(){
        List<String> courseLevel = new ArrayList<>();
        courseLevel = courseRepo.findCourseLevel();
        return courseLevel;
    }

    /*获取排序后的课程*/
    public Map<String,Object> searchSortAllCourse(Integer pageNum, Integer pageSize){
        Map<String, Object> orgs = new HashMap<>(3);
        PageRequest pageable;
        pageable = byPage1(pageNum,pageSize);
        orgs.put("courses", courseRepo.findAll(pageable));
        return orgs;
    }

    /*根据id查找课程（无需权限）*/
    public Map<String,Object> searchCourseById(Integer id){
        Map<String,Object> courses = new HashMap<>(3);
        courses.put("courses",courseRepo.findCourseById(id));
        return courses;
    }

    /*根据机构id查找课程*/
    public Map<String,Object> searchCourseByOrgId(String org_id){
        Map<String,Object> courses = new HashMap<>(3);
        courses.put("courses",courseRepo.findCourseByOrgId(org_id));
        return courses;
    }
}
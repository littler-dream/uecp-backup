package edu.zhku.uecp.service.courses;

import edu.zhku.uecp.model.Course_Type;
import edu.zhku.uecp.model.Course_TypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class C_TypeService {

    @Autowired
    private Course_TypeRepository c_typeRepo;

    /**
     * 功能：分页，排序
     * @param pageNum 页码（从0计）
     * @param pageSize 一页的大小
     * @return
     */
    public PageRequest byPage(Integer pageNum, Integer pageSize){
        PageRequest pageable;
        //返回第一页(每页pageSize个)查询结果，查询结果按名字升序排列
        List<Sort.Order> orders = new ArrayList<>();
        Sort sort;
        orders.clear();
        orders.add(new Sort.Order(Sort.Direction.ASC, "type"));
        sort = Sort.by(orders);
        pageable = PageRequest.of(pageNum, pageSize, sort);
        return pageable;
    }

    /*随机获取最高x位数的数字(不超16位)*/
    public int getNumber(Integer x){
        String random = Math.random()+"";
        String suijishu = random.substring(3, 3+x);
        int result = Integer.parseInt(suijishu);
        return result;
    }

    /*根据id判断是否存在*/
    public boolean isExit(Integer id){
        return c_typeRepo.existsById(id);
    }

    /*添加课程类型*/
    public int addCourseType(Course_Type c_type){
        int id = getNumber(7);
        c_type.setId(id);
        c_typeRepo.save(c_type);
        return id;
    }

    /*删除课程类型*/
    public void deleteCourseType(Integer id){
        c_typeRepo.deleteById(id);
    }

    /*修改课程类型*/
    public String updateCourseType(Integer id, Course_Type c_type){
        c_typeRepo.save(c_type);
        String type = c_type.getType();
        return type;
    }

    /*获取所有课程类型*/
    public Map<String, Object> getAllType(){
        Map<String, Object> courseType = new HashMap<>(3);
        courseType.put("types", c_typeRepo.findAll());
        return courseType;
    }

    /*分页 获取所有课程类型*/
    public Map<String, Object> getAllTypeByPage(Integer pageNum, Integer pageSize){
        Map<String, Object> courseType = new HashMap<>(3);
        PageRequest pageable = byPage(pageNum, pageSize);//
        courseType.put("types", c_typeRepo.findAll(pageable));
        return courseType;
    }

    /*根据关键词搜索课程类型*/
    public Map<String, Object> getTypesByName(String name){
        Map<String, Object> courseTypes = new HashMap<>(3);
        name = "%"+name+"%";
        courseTypes.put("types", c_typeRepo.findByName(name));
        return courseTypes;
    }
}
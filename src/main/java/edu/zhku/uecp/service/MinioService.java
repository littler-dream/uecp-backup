package edu.zhku.uecp.service;

import com.alibaba.fastjson.JSONObject;
import edu.zhku.uecp.model.FileEntity;
import edu.zhku.uecp.model.FileRepository;
import io.minio.*;
import io.minio.http.Method;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.net.http.HttpResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@Service
public class MinioService {
    @Autowired
    private FileRepository fileRepository;
    @Autowired
    private MinioClient client;
    private static String bucketName;


    /**
     * 创建bucket
     * @param bucketName bucket名称
     */
    @SneakyThrows
    public void createBucket(String bucketName) {
        /**
         * bucket权限-读写
         */
        final String READ_WRITE = "{\"Version\":\"2012-10-17\",\"Statement\":[{\"Effect\":\"Allow\",\"Principal\":{\"AWS\":[\"*\"]},\"Action\":[\"s3:GetBucketLocation\",\"s3:ListBucket\",\"s3:ListBucketMultipartUploads\"],\"Resource\":[\"arn:aws:s3:::" + bucketName + "\"]},{\"Effect\":\"Allow\",\"Principal\":{\"AWS\":[\"*\"]},\"Action\":[\"s3:DeleteObject\",\"s3:GetObject\",\"s3:ListMultipartUploadParts\",\"s3:PutObject\",\"s3:AbortMultipartUpload\"],\"Resource\":[\"arn:aws:s3:::" + bucketName + "/*\"]}]}";

        Boolean found = client.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build());
        if (!found) {
            client.makeBucket(
                    MakeBucketArgs.builder()
                            .bucket(bucketName)
                            .build());
            //设置可读可写
            client.setBucketPolicy(
                    SetBucketPolicyArgs.builder()
                            .bucket(bucketName)
                            .config(READ_WRITE)
                            .build());
        }
    }

    public String judgType(MultipartFile file){
        String [] type = {"text","image","audio","video"};
        String fileType = file.getContentType();
        //截取“/”前部分
        String str = fileType.substring(0,fileType.indexOf("/"));
        int i = 0;
        while (i < type.length){
            if (str.equals(type[i])) {
                return type[i];
            }
            i++;
        }
        return "others";
    }
    /**
     * 创建上传文件对象的外链
     * @param bucketName 存储桶名称
     * @param objectName 欲上传文件对象的名称
     * 过期时间(分钟) 最大为7天 超过7天则默认最大值
     * @return uploadUrl
     */
    @SneakyThrows
    public String createUploadUrl(String bucketName,String objectName){
        return client.getPresignedObjectUrl(
                GetPresignedObjectUrlArgs.builder()
                        .method(Method.GET)
                        .bucket(bucketName)
                        .object(objectName)
                        .build()
        );
    }


    /**
     * 上传文件
     *
     * @param file
     * @return
     */
    public String uploadFile(MultipartFile file) throws Exception {
        FileEntity filemodel = new FileEntity();
        //判断类型
        bucketName=judgType(file);
        // 判断存储桶是否存在
        createBucket(bucketName);
        // 构建日期路径, 例如：目标文件夹/2020/10/31/文件名
        String filePath = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        // 文件名
        String fileName = file.getOriginalFilename();
        //类型
        String fileType = file.getContentType();
        // 后缀名
        String suffixName = fileName.substring(fileName.lastIndexOf("."));
        // 新文件名称
        String newFileName = UUID.randomUUID().toString() + suffixName;
        //文件路径
        String objectName = filePath + '/' + newFileName;
        // 开始上传
        InputStream in = file.getInputStream();
        client.putObject(PutObjectArgs.builder()
                .stream(file.getInputStream(), file.getSize(),PutObjectArgs.MIN_MULTIPART_SIZE)
                .object(objectName)
                .contentType(fileType)
                .bucket(bucketName)
                .build());

        //实体类填入信息
        filemodel.setSize(file.getSize());//文件大小
        filemodel.setFileName(fileName);
        filemodel.setSuffixName(suffixName);
        filemodel.setNewName(newFileName);
        filemodel.setPathName(client.getObjectUrl(bucketName,objectName));
        filemodel.setType(fileType);
        fileRepository.save(filemodel);
        in.close();
//        return ENDPOINT + "/" + bucketName + "/" + objectName;
//        return  createUploadUrl(bucketName,objectName);
        return filemodel.getPathName();
    }

}

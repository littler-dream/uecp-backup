package edu.zhku.uecp.service;

import com.alibaba.fastjson.JSONObject;
import edu.zhku.uecp.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class RoleService {
    @Autowired
    private RolePermissionRepository rolePermissionRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private PermissionRepository permissionRepository;

    /**
     * 功能：分页，排序
     * @param pageNum 页码
     * @param pageSize 一页的大小
     * @return
     */
    public PageRequest byPage(Integer pageNum, Integer pageSize){
        PageRequest pageable;
        //返回第一页(每页pageSize个)查询结果，查询结果按角色降序排列
        List<Sort.Order> orders = new ArrayList<>();
        Sort sort;

        orders.clear();
        orders.add(new Sort.Order(Sort.Direction.DESC, "identity"));
        sort = Sort.by(orders);
        pageable = PageRequest.of(pageNum, pageSize, sort);
        return pageable;
    }

    public Boolean isSuperAdm(Integer identity) {
        return identity == 128;
    }

    public Boolean isEx(Integer identity) {
        return roleRepository.existsById(identity);
    }

    public Map<String, Object> findAllRoleNoPage() {
        Map<String, Object> roles = new HashMap<>(3);
        List<Role> all = roleRepository.findAll();
        roles.put("roles", all);
        return roles;
    }

    public Map<String, Object> findAllRole(Integer pageNum, Integer pageSize) {
        Map<String, Object> roles = new HashMap<>(3);
        PageRequest pageable;
        pageable = byPage(pageNum, pageSize);
        roles.put("roles", roleRepository.findAll(pageable));
        return roles;
    }

    public Role findRoleByIdentity(Integer identity) {
        return roleRepository.findById(identity).get();
    }


    public void addRole(Role role2) {
        roleRepository.save(role2);
    }

    public void delRole(Integer id) {
        roleRepository.deleteById(id);
    }

    public void upRole(Role role, Integer identity) {
        role.setIdentity(identity);
        roleRepository.save(role);
    }

    public Map<String, Object> findAllPermis() {
        Map<String, Object> permissions = new HashMap<>(3);
        permissions.put("permissions", permissionRepository.findAll());
        return permissions;
    }
}

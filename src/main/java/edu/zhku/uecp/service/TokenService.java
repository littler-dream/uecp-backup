package edu.zhku.uecp.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import edu.zhku.uecp.model.User;
import edu.zhku.uecp.model.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class TokenService {
    @Autowired
    private UserRepository userRepo;

    public User getVerifiedUserByIdToken(Integer usrId, String token) {
        User usr;
        try {
            usr = userRepo.findById(usrId).get();
            if (usr == null)
                return null;
            if ( verify(token, usr.getId(), usr.getTokenSecretKey()) != 0)
                return null;
            return usr;
        } catch (java.lang.Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }
    public String sign(User usr) {
        //创建JWT令牌token
        long EXPIRE_DATE=24*60*60*1000;
        Date date = new Date(System.currentTimeMillis() + EXPIRE_DATE);
        //秘钥及加密算法
        String secretKey = usr.getTokenSecretKey(); //默认"ZCEQIUBFKSJBFJH2020BQWE";
        Algorithm algorithm = Algorithm.HMAC256(secretKey);
        Map<String, Object> header = new HashMap<>(2);
        header.put("Type", "Jwt");
        header.put("alg", "HS256");
        // JWT携带userID等信息，最后生成加密签名
        String strToken = JWT.create()
                .withHeader(header)
                .withClaim("userId", usr.getId())
                .withExpiresAt(date)
                .sign(algorithm);
        return strToken;
    }

    public Integer verify(String token, Integer usrId, String secretKey) {
        if (token.substring(0,7).equals("Bearer "))
            token = token.substring(7);
        DecodedJWT jwt = JWT.decode(token);
        Integer id = jwt.getClaim("userId").asInt();
        if (id != usrId) {
            return 2;   //token非登录用户的token
        }
        Algorithm algorithm = Algorithm.HMAC256(secretKey);
        try {
            JWTVerifier verifier = JWT.require(algorithm).build();
            verifier.verify(token);
        }catch (JWTVerificationException e) {
            return 1;   //token校验失败，签名不对;
        }
        return 0;
    }

    public Integer getUserId(String token) {
        if (token.substring(0, 7).equals("Bearer "))
            token = token.substring(7);
        DecodedJWT jwt = JWT.decode(token);
        Integer id = jwt.getClaim("userId").asInt();
        return id;
    }
}
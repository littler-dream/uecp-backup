package edu.zhku.uecp.service;

import edu.zhku.uecp.model.User;
import edu.zhku.uecp.model.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AdmCenterService {
    @Autowired
    private UserRepository userRepo;

    /**
     * 功能：分页，排序
     *
     * @param pageNum  页码
     * @param pageSize 一页的大小
     * @return
     */
    public PageRequest byPage(Integer pageNum, Integer pageSize) {
        PageRequest pageable;
        //返回第一页(每页pageSize个)查询结果，查询结果按角色降序排列
        List<Sort.Order> orders = new ArrayList<>();
        Sort sort;

        orders.clear();
        orders.add(new Sort.Order(Sort.Direction.DESC, "role"));
        sort = Sort.by(orders);
        pageable = PageRequest.of(pageNum, pageSize, sort);
        return pageable;
    }

    public boolean isSuperAdm(Integer id) {
        User user = userRepo.existsSuperAdm(id);
        if (user != null) {
            return true;
        }
        return false;
    }

    public Map<String, Object> findAllAdmUsersNoPage() {
        Map<String, Object> orgs = new HashMap<>(3);
        orgs.put("admUsers", userRepo.findAllAdmUsers());
        return orgs;
    }

    public Map<String, Object> findAllAdmUsers(Integer pageNum, Integer pageSize) {
        Map<String, Object> orgs = new HashMap<>(3);
        PageRequest pageable;
        pageable = byPage(pageNum, pageSize);
        orgs.put("admUsers", userRepo.findAllAdmUsers(pageable));
        return orgs;
    }

    //adm用户，存在为真
    public boolean isExistAdmUser(Integer usrId) {
        User user = userRepo.existsByAdmUsers(usrId);
        if (user == null)
            return false;
        return true;
    }

    public User findById(Integer id) {
        return userRepo.findById(id).get();
    }

    public Integer upAdmUser(Integer id, User user) {
        if (userRepo.findById(id).get().getRole() != user.getRole()) {
            return 1;
        } else {
            user.setId(id);
            userRepo.save(user);
        }
        return 0;
    }

    public Integer upAdmUserBySuperAdm(Integer id, User user) {
        user.setId(id);
        userRepo.save(user);
        return 0;
    }

    public Integer delAdmUser(Integer id) {
        userRepo.deleteById(id);
        return 0;
    }

    public Integer saveAdmUser(User user) {
        userRepo.save(user);
        return 0;
    }

    public Integer upUserRole(Integer id, Integer role) {
        User user = userRepo.findById(id).get();
        user.setRole(role);
        return 0;
    }
}

package edu.zhku.uecp.service;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import edu.zhku.uecp.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class UnivService {
    @Autowired
    private UnivAdmRepository uaRepo;
    @Autowired
    private DeptAdmRepository daRepo;
    @Autowired
    private UniversaryRepository univRepo;
    @Autowired
    private RegionRepository regRepo;
    //private static int BATCH_COUNT = 50;
    private List<UnivExcel> errXlsUnivs;
    public PageRequest byPage(Integer pageNum,Integer pageSize){
        PageRequest pageable;
        //返回第一页(每页pageSize个)查询结果，查询结果按名字升序排列
        List<Sort.Order> orders = new ArrayList<>();
        Sort sort;

        orders.clear();
        orders.add(new Sort.Order(Sort.Direction.ASC, "schoolName"));
        sort = Sort.by(orders);
        pageable = PageRequest.of(pageNum, pageSize, sort);
        return pageable;
    }

    public Map<String, Object> findAllUnivsNoPage() {
        Map<String, Object> orgs = new HashMap<>(3);
        orgs.put("univs", univRepo.findAll());
        return orgs;
    }

    public Map<String, Object> findAllUnivs(Integer pageNum, Integer pageSize) {
        Map<String, Object> orgs = new HashMap<>(3);
        PageRequest pageable;
        pageable = byPage(pageNum, pageSize);
        orgs.put("univ", univRepo.findAll(pageable));
        return orgs;
    }

    //保存从Excel文件批量导入的高校数据
    public List<UnivExcel> saveBatchUnivs(MultipartFile file) throws IOException {
        errXlsUnivs = new ArrayList<UnivExcel>();
        EasyExcel.read(file.getInputStream(), UnivExcel.class, new AnalysisEventListener<UnivExcel>() {
            @Override
            public void invoke(UnivExcel univExcel, AnalysisContext analysisContext) {
                Universary univ = new Universary();
                if (univExcel.getProvince_name()!=null && univExcel.getCity_name()!=null) {
                    Region city = regRepo.findByProvCityName(univExcel.getProvince_name(), univExcel.getCity_name());
                    if (city != null)
                        univ.setCityId(city.getId());
                }
                univ.setSchoolName(univExcel.getName());
                try {
                    //数据非缓存，逐条插入，速度慢。
                    // 是否要改成数据缓存，每个事务批量插入，保存BATCH_COUNT条记录？若如此，中途插入失败怎么办？
                    univRepo.save(univ);
                }catch (Exception e) {
                    //跳过（多半是数据库已存在同高校名记录），记录异常数据
                    errXlsUnivs.add(univExcel);
                }
            }

            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                //如批量插入，提交保存最后部分数据
            }
        }).sheet().doRead();
        return errXlsUnivs;//返回未保存的Excel记录
    }

    public Map<String, Object> searchUnivByUnivNameNoPage(User usr, String schoolName) {
        Map<String, Object> orgs = new HashMap<>(3);
        PageRequest pageable;
        //如果是系统管理员(usr.getRole() & 0x80)，返回查询到的所有的高校
        if ((usr.getRole() & 0x80) > 0) {
            orgs.put("univ", univRepo.findUnivsByUnivName(schoolName));
        } else { //否则查找用户管理高校
            orgs.put("univ", univRepo.findUnivsByUserUnivName(usr.getId(), schoolName));
        }
        return orgs;
    }

    public Map<String, Object> searchUnivByUnivName(User usr, String schoolName, Integer pageNum, Integer pageSize) {
        Map<String, Object> orgs = new HashMap<>(3);
        PageRequest pageable;
        pageable = byPage(pageNum, pageSize);
        //如果是系统管理员(usr.getRole() & 0x80)，返回查询到的所有的高校
        if ((usr.getRole() & 0x80) > 0) {
            orgs.put("univ", univRepo.findUnivsByUnivName(schoolName, pageable));
        } else { //否则分别查找和添加用户管理高校
            orgs.put("univ", univRepo.findUnivsByUserUnivName(usr.getId(), schoolName, pageable));
        }
        return orgs;
    }
    public Map<String, Object> searchUnivByCityId(User usr, Integer cityId, Integer pageNum, Integer pageSize) {
        Map<String, Object> orgs = new HashMap<>(3);
        PageRequest pageable;
        pageable = byPage(pageNum, pageSize);
        //如果是系统管理员(usr.getRole() & 0x80)，返回查询到的所有的高校
        if ((usr.getRole() & 0x80) > 0) {
            orgs.put("univ", univRepo.findUnivsByCity(cityId, pageable));
        } else { //否则分别查找和添加用户管理高校
            orgs.put("univ", univRepo.findUnivsByUserCity(usr.getId(), cityId, pageable));
        }
        return orgs;
    }

    public Map<String, Object> searchUnivByCityIdNoPage(User usr, Integer cityId) {
        Map<String, Object> orgs = new HashMap<>(3);
        PageRequest pageable;
        //如果是系统管理员(usr.getRole() & 0x80)，返回查询到的所有的高校
        if ((usr.getRole() & 0x80) > 0) {
            orgs.put("univ", univRepo.findUnivsByCity(cityId));
        } else { //否则分别查找和添加用户管理高校
            orgs.put("univ", univRepo.findUnivsByUserCity(usr.getId(), cityId));
        }
        return orgs;
    }

    public void saveOne(Universary univ2) {
        Universary univ = new Universary();
        univ.setSchoolName(univ2.getSchoolName());
        univ.setProvinceName(univ2.getProvinceName());
        univ.setCityName(univ2.getCityName());
        univ.setLevel(univ2.getLevel());
        univRepo.save(univ);
    }

    //存在为真
    public boolean isExist(Integer univId) {
        return univRepo.existsById(univId);
    }

    public Universary findById(Integer univId) {
        return univRepo.findById(univId).get();
    }

    public void delUniv(Integer univId) {
        univRepo.deleteById(univId);
    }

    public String upUniv(Integer univId, Universary univ2) {
        String befName;
        Universary univ = new Universary();
        univ.setSchoolName(univ2.getSchoolName());
        univ.setProvinceName(univ2.getProvinceName());
        univ.setCityName(univ2.getCityName());
        univ.setLevel(univ2.getLevel());
        befName=univRepo.findAllUnivs(univId).getSchoolName();
        univRepo.save(univ);
        return befName;
    }
    //返回查询到的高校下的所有院系
    public Map<String, Object> searchDeptsByUnivId(Integer univId,Integer pageNum,Integer pageSize){
        Map<String, Object> orgs = new HashMap<>(3);
        PageRequest pageable;
        //返回第一页(每页10个)查询结果，查询结果按名字升序排列
        pageable = byPage(pageNum,pageSize);
        orgs.put("depts", daRepo.findDeptByUnivId(univId, pageable));
        return orgs;
    }
}

package edu.zhku.uecp.service;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import edu.zhku.uecp.model.Enterprise;
import edu.zhku.uecp.model.User;
import edu.zhku.uecp.model.UserExcel;
import edu.zhku.uecp.model.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class User2Service {
    @Autowired
    private UserRepository userRepo;
    private List<UserExcel> errXlsUsers;
    /**
     * 功能：分页，排序
     * @param pageNum 页码
     * @param pageSize 一页的大小
     * @return
     */
    public PageRequest byPage(Integer pageNum, Integer pageSize){
        PageRequest pageable;
        //返回第一页(每页pageSize个)查询结果，查询结果按角色降序排列
        List<Sort.Order> orders = new ArrayList<>();
        Sort sort;

        orders.clear();
        orders.add(new Sort.Order(Sort.Direction.DESC, "role"));
        sort = Sort.by(orders);
        pageable = PageRequest.of(pageNum, pageSize, sort);
        return pageable;
    }

    public Map<String,Object> findAllUsersNoPage(){
        Map<String, Object> orgs = new HashMap<>(3);
        List<User> users = userRepo.findAllByRoleBefore();
//        List list= new ArrayList();
//        for (int i = 0; i < users.size(); i++) {
//            userRepo.findAll().get(i).getHeadImage();
//            if(users.get(i).getHeadImage()!=null)
//                System.out.println(users.get(i).getHeadImage());

//        }
//        System.out.println(users);
        orgs.put("users", users);
        return orgs;
    }

    public  Map<String,Object> findAllUsers(Integer pageNum,Integer pageSize){
        Map<String, Object> orgs = new HashMap<>(3);
        PageRequest pageable;
        pageable = byPage(pageNum, pageSize);
        orgs.put("users", userRepo.findAllByRoleBefore(pageable));
        return orgs;
    }

    public User findById(Integer id){
        return userRepo.findById(id).get();
    }

    public Map<String, Object> findByComRole(Integer role, Integer pageNum, Integer pageSize) {
        Map<String, Object> orgs = new HashMap<>(3);
        PageRequest pageable;
        pageable = byPage(pageNum, pageSize);
        orgs.put("users", userRepo.findByComRole(role, pageable));
        return orgs;
    }

    public Map<String, Object> findByComRoleNoPage(Integer role) {
        Map<String, Object> orgs = new HashMap<>(3);
        orgs.put("users", userRepo.findByComRole(role));
        return orgs;
    }

    //存在为真
    public boolean isExist(Integer usrId) {
        return userRepo.existsById(usrId);
    }

    //普通用户，存在为真
    public boolean isExistComUser(Integer usrId) {
        User user = userRepo.existsByIdAndRole(usrId);
        if (user == null)
            return false;
        return true;
    }

    public Map<String, Object> findByLogNameNoPage(String logname) {
        Map<String, Object> orgs = new HashMap<>(3);
        orgs.put("users", userRepo.findByLogName(logname));
        return orgs;
    }

    public Map<String, Object> findByLogName(String logname, Integer pageNum, Integer pageSize) {
        Map<String, Object> orgs = new HashMap<>(3);
        PageRequest pageable;
        pageable = byPage(pageNum,pageSize);
        orgs.put("users", userRepo.findByLogName(logname,pageable));
        return orgs;
    }

    public void saveUser(User user){
        userRepo.save(user);
    }

    public void saveHeadImage(Integer uid, MultipartFile file) throws IOException {
        userRepo.updateHeadImage(uid, file.getBytes());
    }

    public void delUserById(Integer usrId){
        userRepo.deleteById(usrId);
    }

    public String upUser(Integer usrId, User user){
        String befName;
        user.setId(usrId);
        befName=userRepo.findById(usrId).get().getLogName();
        userRepo.save(user);
        return befName;
    }

    public boolean getUserByLogName(String logname){
        User usr;
        usr = userRepo.findByLogname(logname);
        if (usr == null) {
            return false;
        }
        return true;
    }

    public List<UserExcel> saveBatchUsers(MultipartFile file) throws IOException {
        errXlsUsers = new ArrayList<UserExcel>();
        EasyExcel.read(file.getInputStream(), UserExcel.class, new AnalysisEventListener<UserExcel>() {
            @Override
            public void invoke(UserExcel userExcel, AnalysisContext analysisContext) {
                User user = new User();
                user.setName(userExcel.getName());
                user.setLogName(userExcel.getLogName());
                user.setPassword(userExcel.getPassword());
                user.setSex(userExcel.getSex());
                user.setMobilePhone(userExcel.getMobilePhone());
                user.setEmail(userExcel.getEmail());
                try {
                    //数据非缓存，逐条插入，速度慢。
                    // 是否要改成数据缓存，每个事务批量插入，保存BATCH_COUNT条记录？若如此，中途插入失败怎么办？
                    userRepo.save(user);
                }catch (Exception e) {
                    //跳过（多半是数据库已存在同高校名记录），记录异常数据
                    errXlsUsers.add(userExcel);
                }
            }
            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                //如批量插入，提交保存最后部分数据
            }
        }).sheet().doRead();
        return errXlsUsers;//返回未保存的Excel记录
    }
}

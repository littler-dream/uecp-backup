package edu.zhku.uecp.service;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import edu.zhku.uecp.model.Class;
import edu.zhku.uecp.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class ClassService {
    @Autowired
    private ClassRepository classRepo;
    @Autowired
    private DepartmentRepository deptRepo;
    @Autowired
    private DeptAdmRepository daRepo;
    private static int BATCH_COUNT = 30;
    private int total = 0;
    private int count = 0;

    public boolean isExist(Integer classId){
        return classRepo.existsById(classId);
    }

    //批量导入班级信息
    //Read Committed，就是读已提交，一个事务只能看到其他并发的已提交事务所作的修改
    @Transactional(isolation = Isolation.READ_COMMITTED,propagation = Propagation.REQUIRES_NEW)
    public int saveBatchClasses(MultipartFile file) throws IOException {
        List<Class> classList = new ArrayList<>();
        EasyExcel.read(file.getInputStream(), ClassExcel.class, new AnalysisEventListener<ClassExcel>() {
            @Override
            public void invoke(ClassExcel classExcel, AnalysisContext analysisContext) {
                Class cla = new Class();

                Department dept = deptRepo.findByDeptName(classExcel.getDept());
                cla.setDeptId(dept.getId());
                cla.setName(classExcel.getName());
                classList.add(cla);
                total++;
                if(classList.size() >= BATCH_COUNT) {
                    try {
                        classRepo.saveAll(classList);
                        count = total;
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    classList.clear();
                }
            }
            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                //如批量插入，提交保存最后部分数据
                if(classList.size() > 0) {
                    try {
                        classRepo.saveAll(classList);
                        count = total;
                        total = 0;
                    } catch (Exception e) {

                        System.out.println(e.getMessage());
                    }
                }
            }
        }).sheet().doRead();
        return count;//返回保存的记录数
    }


    public void delClass(Integer classId) {
        classRepo.deleteById(classId);
    }

    public String upCla(Integer classId, Class cla2){
        String befName;
        befName=classRepo.findClass(classId).getName();
        Class cla = new Class();
        cla.setDeptId(cla2.getDeptId());
        cla.setId(classId);
        cla.setName(cla2.getName());
        classRepo.save(cla);
        return befName;
    }

    public void saveOne(Class cla2){
        Class cla = new Class();
        cla.setDeptId(cla2.getDeptId());
        cla.setName(cla2.getName());
        classRepo.save(cla);
    }
}

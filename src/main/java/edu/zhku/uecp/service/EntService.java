package edu.zhku.uecp.service;

import com.alibaba.druid.sql.dialect.oracle.ast.stmt.OracleGotoStatement;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import edu.zhku.uecp.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@Service
public class EntService {
    @Autowired
    private  UserRepository usrRepo;
    @Autowired
    private RegionRepository regRepo;
    @Autowired
    private EntAdmRepository eaRepo;
    @Autowired
    private EnterpriseRepository entRepo;

    private int count;//读入Excel数据的行数

    /**
     * 功能：分页，排序
     * @param pageNum 页码
     * @param pageSize 一页的大小
     * @return
     */
    public PageRequest byPage(Integer pageNum,Integer pageSize){
        PageRequest pageable;
        //返回第一页(每页pageSize个)查询结果，查询结果按名字升序排列
        List<Sort.Order> orders = new ArrayList<>();
        Sort sort;

        orders.clear();
        orders.add(new Sort.Order(Sort.Direction.ASC, "name"));
        sort = Sort.by(orders);
        pageable = PageRequest.of(pageNum, pageSize, sort);
        return pageable;
    }

    public  Map<String,Object> findAllEntsNoPage(){
        Map<String, Object> orgs = new HashMap<>(3);
        orgs.put("ents", entRepo.findAll());
        return orgs;
    }

    public  Map<String,Object> findAllEnts(Integer pageNum,Integer pageSize){
        Map<String, Object> orgs = new HashMap<>(3);
        PageRequest pageable;
        pageable = byPage(pageNum,pageSize);
        orgs.put("ents", entRepo.findAll(pageable));
        return orgs;
    }

    public Enterprise findById(Integer id){
        return entRepo.findById(id).get();
    }

    /**
     * 根据企业名模糊查找企业，分页
     * 修改日期：2021-7-14
     */
    public Map<String, Object> searchEntByEntName(User usr, String entName,Integer pageNum,Integer pageSize) {
        Map<String, Object> orgs = new HashMap<>(3);
        PageRequest pageable;

        pageable = byPage(pageNum,pageSize);
        //如果是系统管理员(usr.getRole() & 0x80)，返回查询到的所有的企业
        if ((usr.getRole() & 0x80) > 0) {
            orgs.put("ents", eaRepo.findEntsByEntName(entName, pageable));
        } else { //否则查找用户管理企业
            orgs.put("ents", eaRepo.findEntsByEntName(usr.getId(), entName, pageable));
        }
        return orgs;
    }

    /**
     * 根据企业名模糊查找企业，不分页
     * 修改日期：2021-7-17
     */
    public Map<String, Object> searchEntByEntNameNoPage(User usr, String entName) {
        Map<String, Object> orgs = new HashMap<>(3);
        PageRequest pageable;

        //如果是系统管理员(usr.getRole() & 0x80)，返回查询到的所有的企业
        if ((usr.getRole() & 0x80) > 0) {
            orgs.put("ents", eaRepo.findEntsByEntName(entName));
        } else { //否则查找用户管理企业
            orgs.put("ents", eaRepo.findEntsByEntName(usr.getId(), entName));
        }
        return orgs;
    }

    //根据地名模糊查找企业，分页
    public Map<String, Object> searchEntByCityId(User usr, Integer cityId, Integer pageNum, Integer pageSize) {
        Map<String, Object> orgs = new HashMap<>(3);
        PageRequest pageable;

        pageable = byPage(pageNum,pageSize);
        //如果是系统管理员(usr.getRole() & 0x80)
        if ((usr.getRole() & 0x80) > 0) {
            orgs.put("ents", eaRepo.findEntsByCityId(cityId, pageable));
        } else { //否则查找用户管理企业
            orgs.put("ents", eaRepo.findEntsByCityId(usr.getId(), cityId, pageable));
        }
        return orgs;
    }

    //根据地名模糊查找企业，不分页
    public Map<String, Object> searchEntByCityIdNoPage(User usr, Integer cityId) {
        Map<String, Object> orgs = new HashMap<>(3);
        PageRequest pageable;

        //如果是系统管理员(usr.getRole() & 0x80)
        if ((usr.getRole() & 0x80) > 0) {
            orgs.put("ents", eaRepo.findEntsByCityId(cityId));
        } else { //否则查找用户管理企业
            orgs.put("ents", eaRepo.findEntsByCityId(usr.getId(), cityId));
        }
        return orgs;
    }

    //批量导入企业信息
    public int saveBatchEnts(MultipartFile file) throws IOException {
        count = 0;
        EasyExcel.read(file.getInputStream(), EntExcel.class, new AnalysisEventListener<EntExcel>() {
            @Override
            public void invoke(EntExcel entExcel, AnalysisContext analysisContext) {
                Enterprise ent = new Enterprise();
                if (entExcel.getProv() != null && entExcel.getCity() != null) {
                    Region city = regRepo.findByProvCityName(entExcel.getProv(), entExcel.getCity());
                    if (city != null)
                        ent.setCity(city.getId());
                }
                ent.setName(entExcel.getName());
                ent.setSname(entExcel.getSname());
                ent.setAddress(entExcel.getAddress());
                ent.setUscc(entExcel.getUscc());
                try {
                    //数据非缓存，逐条插入，速度慢。
                    // 是否要改成数据缓存，每个事务批量插入，保存BATCH_COUNT条记录？若如此，中途插入失败怎么办？
                    entRepo.save(ent);
                    count++;
                } catch (Exception e) {
                    //跳过（数据库已存在同企业名记录）
                }
            }

            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                //如批量插入，提交保存最后部分数据
            }
        }).sheet().doRead();
        return count;//返回保存的记录数
    }

    //存在为真
    public boolean isExist(Integer entId){
        return entRepo.existsById(entId);
    }

    public void delEnt(Integer entId) {
        entRepo.deleteById(entId);
    }

    public String upEnt(Integer entId,Enterprise ent2){
        String befName;
        Enterprise ent = new Enterprise();
        ent.setId(entId);
        ent.setName(ent2.getName());
        ent.setCity(ent2.getCity());
        ent.setSname(ent2.getSname());
        ent.setAddress(ent2.getAddress());
        ent.setUscc(ent2.getUscc());
        ent.setIntroduction(ent2.getIntroduction());
        befName=entRepo.findEnt(entId).getName();
        entRepo.save(ent);
        return befName;
    }

    public void saveOne(Enterprise ent2){
        Enterprise ent = new Enterprise();
//        ent.setCity(Integer.parseInt(map.get("city")));
//        ent.setSname(map.get("sname"));
//        ent.setName(map.get("name"));
//        ent.setAddress(map.get("address"));
//        ent.setUscc(map.get("uscc"));
        ent.setName(ent2.getName());
        ent.setCity(ent2.getCity());
        ent.setSname(ent2.getSname());
        ent.setAddress(ent2.getAddress());
        ent.setUscc(ent2.getUscc());
        ent.setIntroduction(ent2.getIntroduction());
        entRepo.save(ent);
    }
}

package edu.zhku.uecp.service;

import edu.zhku.uecp.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import org.springframework.data.domain.PageRequest;

@Service
public class AdmService {
    @Autowired
    private UserRepository userRepo;
    @Autowired
    private UnivAdmRepository uaRepo;
    @Autowired
    private EntAdmRepository eaRepo;
    @Autowired
    private DeptAdmRepository daRepo;
    @Autowired
    private UniversaryRepository univRepo;
    @Autowired
    private EnterpriseRepository entRepo;

    //获取管理员管理的所有机构信息
    public Map<String, Object> getOrgsByAdmId(User usr) {
        Map<String, Object> orgs = new HashMap<>(3);

        //如果是系统管理员(usr.getRole() & 0x80)，返回所有的高校和企业
        if ((usr.getRole() & 0x80) > 0) {
            orgs.put("univs", univRepo.findAll());
            orgs.put("ents", entRepo.findAll());
            orgs.put("depts", null);
        } else { //否则分别查找和添加用户管理的高校,企业和院系
            orgs.put("univs", uaRepo.findUnivsByAdmId(usr.getId()));
            orgs.put("ents", eaRepo.findEntsByAdmId(usr.getId()));
            orgs.put("depts", daRepo.findDeptsByAdmId(usr.getId()));
        }
        return orgs;
    }

    //修改日期：2021-4-21
    public Map<String, Object> getOrgsOrderById(User usr, Integer pageNum) {
        Map<String, Object> orgs = new HashMap<>(3);
        PageRequest pageable;
        //返回第一页(每页10个)查询结果，查询结果按名字升序排列
        List<Sort.Order> orders = new ArrayList<>();
        Sort sort;

        //如果是系统管理员(usr.getRole() & 0x80)，返回所有的高校和企业
        if ((usr.getRole() & 0x80) > 0) {
            orders.clear();
            orders.add(new Sort.Order(Sort.Direction.ASC,"Id"));
            sort = Sort.by(orders);
            pageable = PageRequest.of(pageNum, 10, sort);
            orgs.put("univs", univRepo.findAll(pageable));
            orgs.put("ents", entRepo.findAll(pageable));
            orgs.put("depts", null);
        } else { //否则分别查找和添加用户管理的高校,企业和院系
            orders.clear();
            orders.add(new Sort.Order(Sort.Direction.ASC,"UnivInfo.Id"));
            sort = Sort.by(orders);
            pageable = PageRequest.of(pageNum, 10, sort);
            orgs.put("univs", uaRepo.findUnivsByAdmId(usr.getId(),pageable));
            orders.clear();
            orders.add(new Sort.Order(Sort.Direction.ASC,"EntInfo.Id"));
            sort = Sort.by(orders);
            pageable = PageRequest.of(pageNum, 10, sort);
            orgs.put("ents", eaRepo.findEntsByAdmId(usr.getId(),pageable));
            orders.clear();
            orders.add(new Sort.Order(Sort.Direction.ASC,"DeptInfo.Id"));
            sort = Sort.by(orders);
            pageable = PageRequest.of(pageNum, 10, sort);
            orgs.put("depts", daRepo.findDeptsByAdmId(usr.getId(),pageable));
        }
        return orgs;
    }

    //修改日期：2021-4-21
    //判断登录用户是否管理员，如是则返回管理员用户，否则null
    public User getAdministratorByUsrId(Integer usrId) {
        User usr;
        usr = userRepo.findById(usrId).get();
        if (usr == null) return null;
        if ((usr.getRole() & 0x80) > 0) //系统管理员
            return usr;
        //判断是否高校管理员
        List<Universary> us = uaRepo.findUnivsByAdmId(usr.getId());
        if (!us.isEmpty())
            return usr;
        //以下还要判断是否企业管理员，方法与判断高校管理员类似
        List<Enterprise> es = eaRepo.findEntsByAdmId(usr.getId());
        if (!es.isEmpty())
            return usr;
        //判断是否高校院系管理员
        List<Department> ds = daRepo.findDeptsByAdmId(usr.getId());
        if (!ds.isEmpty()) {
            return usr;
        }
        return null;    //非管理员
    }

    //判断登录用户是否管理员，如是则返回管理员用户，否则null
    public User getAdministratorByLogname(String logname) {
        User usr;
        usr = userRepo.findByLogname(logname);
        if (usr == null) return null;
        if ((usr.getRole() & 0x80) > 0) //系统管理员
            return usr;
        //判断是否高校管理员
        List<Universary> us = uaRepo.findUnivsByAdmId(usr.getId());
        if (!us.isEmpty())
            return usr;
        //以下还要判断是否企业管理员，方法与判断高校管理员类似
        List<Enterprise> es = eaRepo.findEntsByAdmId(usr.getId());
        if (!es.isEmpty())
            return usr;
        //判断是否高校院系管理员
        List<Department> ds = daRepo.findDeptsByAdmId(usr.getId());
        if (!ds.isEmpty()) {
            return usr;
        }
        return null;    //非管理员
    }
    //判断是否为企业管理员或者系统管理员
    public User getEntAdminByUsrId(Integer usrId) {
        User usr;
        usr = userRepo.findById(usrId).get();
        if (usr == null) return null;
        if ((usr.getRole() & 0x80) > 0) //系统管理员
            return usr;
        //企业管理员
        List<Enterprise> es = eaRepo.findEntsByAdmId(usr.getId());
        if (!es.isEmpty())
            return usr;
        return null;    //非管理员
    }

    //判断是否为高校管理员或者系统管理员
    public User getUnivAdminByUsrId(Integer usrId) {
        User usr;
        usr = userRepo.findById(usrId).get();
        if (usr == null) return null;
        if ((usr.getRole() & 0x80) > 0) //系统管理员
            return usr;
        //高校管理员
        List<Universary> us = uaRepo.findUnivsByAdmId(usr.getId());
        if (!us.isEmpty())
            return usr;
        return null;    //非管理员
    }

    //判断是否为院系管理员或者系统管理员
    public User getDeptAdminByUsrId(Integer usrId) {
        User usr;
        usr = userRepo.findById(usrId).get();
        if (usr == null) return null;
        if ((usr.getRole() & 0x80) > 0) //系统管理员
            return usr;
        //院系管理员
        List<Department> ds = daRepo.findDeptsByAdmId(usr.getId());
        if (!ds.isEmpty()) {
            return usr;
        }
        return null;    //非管理员
    }


}

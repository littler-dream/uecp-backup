package edu.zhku.uecp.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.config.annotation.*;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")                  // 设置
                //设置允许跨域来源模式为*---所有域。则限制浏览器:1、禁止用户接收用户自定义header(如以下Challege)；2、禁止浏览器发送cookie
                //.allowedOriginPatterns("*")
                // 设置允许跨域来源为某些特定域，则限制浏览器：只发送cookie来源为以下特定域的cookie给服务器。
                // 以下为前端服务器url--记得提醒前端开发人员！！
                // 如visual code的launch.json的configurations中设置url为http://localhost:8080
                .allowedOrigins("http://localhost:8080", "http://localhost:80", "http://114.55.218.93")
                // 是否允许证书(浏览器发送cookie和HTTP认证信息(--Authorization头？))
                .allowCredentials(true)
                .allowedMethods("GET", "POST", "DELETE", "PUT", "HEAD", "OPTIONS")   // 设置允许浏览器请求的方法
                .allowedHeaders("*")                              // 设置允许浏览器发送的header属性
                .exposedHeaders("Origin", "X-Requested-With", "Content-Type", "Accept", "Challenge") //设置允许暴露给浏览器的Header
                .maxAge(3600);                                   // 跨域允许时间
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        //静态资源访问
        registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");

        /** 配置knife4j 显示文档 */
        registry.addResourceHandler("doc.html")
                .addResourceLocations("classpath:/META-INF/resources/");

    }
    /**
     * 使用重定向的方式使用户可以通过ip:port的方式直接访问swagger2
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        //默认地址（可以是页面或后台请求接口）
        registry.addViewController("/").setViewName("redirect:/swagger-ui/index.html");
        //设置过滤优先级最高
        registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
    }

}
package edu.zhku.uecp.config;

import io.minio.MinioClient;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidPortException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * minio 核心配置类
 */
@Configuration
public class MinioConfig {

    private static String ENDPOINT = "http://114.55.218.93:9000";  //minio服务的IP端口
    private static String ACCESSKEY = "minioadmin";
    private static String SECRETKEY = "minioadmin";

    /**
     * 获取 MinioClient
     *
     * @return
     * @throws InvalidPortException
     * @throws InvalidEndpointException
     */
    @Bean
    public MinioClient minioClient() throws InvalidPortException, InvalidEndpointException {
        return new MinioClient(ENDPOINT, ACCESSKEY, SECRETKEY);
    }
}



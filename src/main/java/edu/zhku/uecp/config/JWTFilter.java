package edu.zhku.uecp.config;

import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.exceptions.JWTDecodeException;
import edu.zhku.uecp.model.JWTToken;
import edu.zhku.uecp.model.User;
import edu.zhku.uecp.service.TokenService;
import edu.zhku.uecp.service.UnivService;
import edu.zhku.uecp.service.UserService;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.web.filter.authc.BasicHttpAuthenticationFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JWTFilter extends BasicHttpAuthenticationFilter {
    @Autowired
    private TokenService tokenService;
    @Autowired
    private UserService userService;

//    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    /**
     * 判断用户是否想要登入。
     * 检测header里面是否包含Authorization字段即可
     */
    @Override
    protected boolean isLoginAttempt(ServletRequest request, ServletResponse response) {
        HttpServletRequest req = (HttpServletRequest) request;
        String authorization = req.getHeader("Authorization");

        if (authorization != null) {
            if (!authorization.substring(0, 7).equals("Bearer "))
                authorization = null;
            String[] parts = authorization.split("\\.");
//        if (parts.length == 2 && authorization.endsWith(".")) {
//            parts = new String[]{parts[0], parts[1], ""};
//        }
            if (parts.length != 3) {
//            json.put("ret", 401);
//            json.put("msg", String.format("The token was expected to have 3 parts, but got %s.", parts.length));
//            response.getWriter().write(json.toString());
//            return false;
                authorization = null;
            }
        }
        return authorization != null;
    }

    /**
     *
     */
    @Override
    protected boolean executeLogin(ServletRequest request, ServletResponse response) throws Exception {
        response.setCharacterEncoding("UTF-8");
//        response.reset();
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        JSONObject json = new JSONObject();
        String authorization = httpServletRequest.getHeader("Authorization");
//        Integer userId = Integer.valueOf(httpServletRequest.getHeader("userId"));
//        User userBean = userService.getUserById(userId);
//        if (tokenService.verify(authorization, userId, userBean.getTokenSecretKey()) != 0) {
//            json.put("ret", 500);
//            json.put("msg", "不是该用户token");
//            response.getWriter().write(json.toString());
//            return false;
//        }
        JWTToken token = new JWTToken(authorization);
        try {

            // 提交给realm进行登入，如果错误他会抛出异常并被捕获
            getSubject(request, response).login(token);
            // 如果没有抛出异常则代表登入成功，返回true
            return true;
        } catch (AuthenticationException e) {
            json.put("ret", 401);
            json.put("msg", "没有访问权限，原因是:" + e.getMessage());
            response.getWriter().write(json.toString());
            return false;
        }
    }

    /**
     * 这里我们详细说明下为什么最终返回的都是true，即允许访问
     * 例如我们提供一个地址 GET /article
     * 登入用户和游客看到的内容是不同的
     * 如果在这里返回了false，请求会被直接拦截，用户看不到任何东西
     * 所以我们在这里返回true，Controller中可以通过 subject.isAuthenticated() 来判断用户是否登入
     * 如果有些资源只有登入用户才能访问，我们只需要在方法上面加上 @RequiresAuthentication 注解即可
     * 但是这样做有一个缺点，就是不能够对GET,POST等请求进行分别过滤鉴权(因为我们重写了官方的方法)，但实际上对应用影响不大
     */
    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        if (isLoginAttempt(request, response)) {
            try {
                executeLogin(request, response);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        return true;
    }

//    /**
//     * 对跨域提供支持
//     */
//    @Override
//    protected boolean preHandle(ServletRequest request, ServletResponse response) throws Exception {
//        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
//        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
//        httpServletResponse.setHeader("Access-control-Allow-Origin", httpServletRequest.getHeader("Origin"));
//        httpServletResponse.setHeader("Access-Control-Allow-Methods", "GET,POST,OPTIONS,PUT,DELETE");
//        httpServletResponse.setHeader("Access-Control-Allow-Headers", httpServletRequest.getHeader("Access-Control-Request-Headers"));
//        // 跨域时会首先发送一个option请求，这里我们给option请求直接返回正常状态
//        if (httpServletRequest.getMethod().equals(RequestMethod.OPTIONS.name())) {
//            httpServletResponse.setStatus(HttpStatus.OK.value());
//            return false;
//        }
//        return super.preHandle(request, response);
//    }

}

package edu.zhku.uecp.config;

import org.apache.shiro.authz.AuthorizationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class MyExceptionHandler {

    @ExceptionHandler(value = AuthorizationException.class)
    @ResponseBody
    public Map<String, String> handleException(AuthorizationException e) {
        //e.printStackTrace();
        Map<String, String> result = new HashMap<String, String>();
        result.put("ret", "401");
        //获取错误中中括号的内容
        String message = e.getMessage();

        //判断是角色错误还是权限错误
        if (message.contains("role")) {
            String msg=message.substring(message.indexOf("[")+1,message.indexOf("]"));
            result.put("msg", "没有"+ msg +"角色");
        } else if (message.contains("permission")) {
            String msg=message.substring(message.indexOf("[")+1,message.indexOf("]"));
            result.put("msg", "没有"+ msg +"权限");
        } else {
            result.put("ret", "500");
            result.put("msg", "没有登录或者token错误");
        }
        return result;
    }
}


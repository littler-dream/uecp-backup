package edu.zhku.uecp.config;

import edu.zhku.uecp.model.*;
import edu.zhku.uecp.service.TokenService;
import edu.zhku.uecp.service.UserService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Component
public class AuthRealm extends AuthorizingRealm {
    @Autowired
    private UserService userService;
    @Autowired
    private TokenService tokenService;
    @Autowired
    private RolePermissionRepository rolePermissionRepository;


    /**
     * 大坑！，必须重写此方法，不然Shiro会报错
     */
    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof JWTToken;
    }

    /**
     * 只有当需要检测用户权限的时候才会调用此方法，例如checkRole,checkPermission之类的
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        Integer userId = tokenService.getUserId(principals.toString());
        User user = userService.getUserById(userId);
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        simpleAuthorizationInfo.addRole(user.getRoleInfo().getRoleName());

        List<Permission> permissionsList = user.getRoleInfo().getPermissionList();
//        List<Permission> permissionsList = rolePermissionRepository.findpermissionsByRoleId(user.getRoleInfo().getId());
        for (Permission permission : permissionsList) {
            //2.1.1添加权限
            simpleAuthorizationInfo.addStringPermission(permission.getPermission());
        }
        return simpleAuthorizationInfo;
    }

    /**
     * 默认使用此方法进行用户名正确与否验证，错误抛出异常即可。
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken auth) throws AuthenticationException {
        String token = (String) auth.getCredentials();
        if (token == null)
            throw new AuthenticationException("token为空,或者不合法");
        // userId
        Integer userId = tokenService.getUserId(token);
        if (userId == null) {
            throw new AuthenticationException("token invalid");
        }

        User userBean = userService.getUserById(userId);
        if (userBean == null) {
            throw new AuthenticationException("User didn't existed!");
        }

        if (tokenService.verify(token, userId, userBean.getTokenSecretKey()) != 0) {
            throw new AuthenticationException("UserId error");
        }

        return new SimpleAuthenticationInfo(token, token, "my_realm");
    }
}


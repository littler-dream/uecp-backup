package edu.zhku.uecp;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import springfox.documentation.oas.annotations.EnableOpenApi;

import javax.sql.DataSource;

@EnableOpenApi
@SpringBootApplication
public class UecpApplication {
    public static void main(String[] args) {
        SpringApplication.run(UecpApplication.class, args);
    }
}
package edu.zhku.uecp.model;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RolePermissionRepository extends JpaRepository<RolePermission, RolePermissionPrimaryKey> {
    @Query("select rp.permissionInfo from RolePermission rp where rp.roleId=?1")
    List<Permission> findpermissionsByRoleId(Integer roleId);
    @Query("select rp.permissionInfo from RolePermission rp where rp.roleId=?1")
    Page<Permission> findpermissionsByRoleId(Integer roleId, Pageable pageable);
}

package edu.zhku.uecp.model;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface UniversaryRepository extends JpaRepository<Universary, Integer> {
    //说明：以下不重写的话，findAll(Pageable pageable)默认方法将会把pageable的
    // Sort排序属性字符串(如"Id")自动转换成小写(如"id")，
    // 与注入代码中的Universary属性名(如"Id")可能不一致，会导致异常。
    @Query("select u from Universary u")
    Page<Universary> findAll(Pageable pageable);

    @Query("select u from Universary u where u.id=?1")
    Universary findAllUnivs(Integer entId);

    @Query("select u from Universary u where u.schoolName like CONCAT('%',:name,'%')")
    Page<Universary> findUnivsByUnivName(@Param("name") String name, Pageable pageable);

    @Query("select u from Universary u where u.schoolName like CONCAT('%',:name,'%') and u.id=any( select ua.univId from UnivAdm ua where ua.userId=:userid) ")
    Page<Universary> findUnivsByUserUnivName(@Param("userid") Integer id, @Param("name") String name, Pageable pageable);

    @Query("select u from Universary u where u.schoolName like CONCAT('%',:name,'%')")
    List<Universary> findUnivsByUnivName(@Param("name") String name);

    @Query("select u from Universary u where u.schoolName like CONCAT('%',:name,'%') and u.id=any( select ua.univId from UnivAdm ua where ua.userId=:userid) ")
    List<Universary> findUnivsByUserUnivName(@Param("userid") Integer id, @Param("name") String name);

    @Query("select u from Universary u where u.cityId=?1")
    Page<Universary> findUnivsByCity(Integer cityId, Pageable pageable);

    @Query("select u from Universary u where u.cityId=?2 and u.id in (select ua.univId from UnivAdm ua where ua.userId=?1)")
    Page<Universary> findUnivsByUserCity(Integer uid, Integer cityId, Pageable pageable);

    @Query("select u from Universary u where u.cityId=?1")
    List<Universary> findUnivsByCity(Integer cityId);

    @Query("select u from Universary u where u.cityId=?2 and u.id in (select ua.univId from UnivAdm ua where ua.userId=?1)")
    List<Universary> findUnivsByUserCity(Integer uid, Integer cityId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update Universary u set u.schoolName=?2 where u.schoolId=?1")
    void update(Integer schoolId, String schoolName);
}


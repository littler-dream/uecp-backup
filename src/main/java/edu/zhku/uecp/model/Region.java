package edu.zhku.uecp.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "region")
public class Region {
    @javax.persistence.Id
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "pid")
    private Integer pId;
    @Column(name = "sname")
    private String sname;
    @Column(name = "level")
    private Integer level;
    @Column(name = "citycode")
    private String cityCode;
    @Column(name = "yzcode")
    private String yzCode;
    @Column(name = "mername")
    private String merName;
    @Column(name = "lng")
    private float longitude;
    @Column(name = "lat")
    private float latitude;
    @Column(name = "pinyin")
    private String pinyin;
}
package edu.zhku.uecp.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import javax.persistence.*;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "course_video")
@ApiModel(value = "Course_video" , description = "课程视频实体类")
public class Course_Video {
    @Id
    @Column(name = "id")
    @ApiModelProperty(value = "视频id",name = "id",hidden = true)
    private Integer id;

    @Column(name = "course_id")
    @NotNull
    @ApiModelProperty(value = "课程id（必须是已有课程的id）",required = true )
    private Integer courseId;

    @Column(name = "name")
    @NotNull
    @ApiModelProperty(value = "视频名称",required = true )
    private String name;

    @Column(name = "video")
    @NotNull
    @ApiModelProperty(value = "视频的url地址",required = true )
    private String video;

}
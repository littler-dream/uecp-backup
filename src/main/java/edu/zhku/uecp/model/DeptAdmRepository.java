package edu.zhku.uecp.model;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface DeptAdmRepository extends JpaRepository<DeptAdm, DeptAdmPrimaryKey> {
    @Query("select da.deptInfo from DeptAdm da where da.userId=?1")
    List<Department> findDeptsByAdmId(Integer usrId);
    @Query("select da.deptInfo from DeptAdm da where da.userId=?1")
    Page<Department> findDeptsByAdmId(Integer usrId, Pageable pageable);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update Class c set c.name=?2 where c.id=?1")
    void update(Integer id, String name);

    @Query("select d from Department d where d.univId=?1")
    Page<Department> findDeptByUnivId(Integer univId,Pageable pageable);
}
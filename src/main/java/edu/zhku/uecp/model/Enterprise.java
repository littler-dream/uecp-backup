package edu.zhku.uecp.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Tolerate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Date;

@Data
@Entity
@Table(name = "ent")
@ApiModel(value = "Enterprise" , description = "企业实体类")
public class Enterprise {
    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(value = "id",name = "id" ,hidden = true)
    private Integer id;

    @Column(name = "name")
    @NotNull
    @ApiModelProperty(value = "企业名称", name = "name", example = "广东美的有限公司", required = true)
    private String name;

    @Column(name = "sname")
    @ApiModelProperty(value = "企业名称的简称", name = "sname", example = "美的")
    private String sname;

    @Column(name = "city")
    @ApiModelProperty(value = "所在城市编码", name = "city", example = "440100", required = true)
    private Integer city;

    @Column(name = "address")
    @ApiModelProperty(value = "地址", name = "address", example = "海珠区东沙街24号")
    private String address;


    @Column(name = "uscc")
    @ApiModelProperty(value = "统一社会信用代码", name="uscc", example = "0000000")
    private String uscc;//统一社会信用代码

    @Column(name = "province_name", insertable = false, updatable = false)
    @ApiModelProperty(value = "省名（不用填）由触发器生成", name="provinceName")
    private String provinceName;

    @Column(name = "city_name", insertable = false, updatable = false)
    @ApiModelProperty(value = "城市名（不用填）由触发器生成", name="cityName")
    private String cityName;

    //以下两属性只读，其值由表触发器修改
    @Column(name = "creattime", insertable = false, updatable = false)
    @ApiModelProperty(value = "创建时间（不用填）由触发器生成", name = "createTime")
    private Date createTime;


    @Column(name = "moditime", insertable = false, updatable = false)
    @ApiModelProperty(value = "修改时间（不用填）由触发器生成",name = "modiTime")
    private Date modiTime;

    @Column(name = "introduction")
    @ApiModelProperty(value = "企业简介", name = "introduction", example = "略")
    private String introduction;

    @ManyToOne
    @JoinColumn(name="city", insertable=false, updatable=false)
    @ApiModelProperty(name = "cityInfo",hidden = true)
    private Region cityInfo;
}
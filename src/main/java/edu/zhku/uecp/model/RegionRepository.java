package edu.zhku.uecp.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RegionRepository extends JpaRepository<Region, Integer> {
    @Query("select c from Region c where c.name=?2 and c.pId in (select p.id from Region p where p.name=?1 and p.level=1)")
    Region findByProvCityName(String provName, String cityName);
    @Query(value = "select p from Region p where p.level=1 order by p.pinyin")
    List<Region> findAllProvsOrderByPinyin();
    @Query(value = "select c from Region c where c.pId=?1 order by c.pinyin")
    List<Region> findAllCitiesInProvOrderByPinyin(Integer PId);
}
package edu.zhku.uecp.model;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data   //由lombok提供，自动生成get、set等方法
@Entity //告诉JPA这是一个实体类（和数据表映射的类）
@Table(name = "permis")
public class Permission {
    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(value = "id",name = "id" ,example = "6", required = true)
    private Integer id;

    @Column(name = "name")
    @NotNull
    @ApiModelProperty(value = "权限中文名", name = "name", example = "用户管理", required = true)
    private String name;

    @Column(name = "permission")
    @ApiModelProperty(value = "权限字符串名", name = "permission", example = "user:view")
    private String permission;
//    @Column(name = "resource_type", columnDefinition="enum")
//    private Resource_type resourceType;
//
//    public enum Resource_type {
//        menu,
//        button
//    }
//

    @Column(name = "resource_type")
    @ApiModelProperty(value = "资源类型(menu或者button)", name = "resource_type", example = "menu")
    private String resource_type;
    @Column(name = "parent_id")
    @ApiModelProperty(value = "父id", name = "parentId", example = "0")
    private Integer parentId;
    @Column(name = "menu_id")
    @ApiModelProperty(value = "菜单级别(0,1,2)", name = "menuId", example = "0")
    private Integer menuId;

//    @ManyToMany(mappedBy = "permissionList",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
//    private List<Role> roleList;
}

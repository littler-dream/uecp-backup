package edu.zhku.uecp.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Date;

@Data
@Entity
@Table(name = "file")
public class FileEntity {
    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "path_name")//文件路径
    @NotNull
    private String pathName;

    @Column(name = "file_name")//本来的名字
    @NotNull
    private String fileName;

    @Column(name = "suffix_name")//后缀名
    @NotNull
    private String suffixName;

    @Column(name = "new_name")//存储的名字
    @NotNull
    private String newName;

    @Column(name = "size")//大小
    private long size;
    @Column(name = "creator")//创建者
    private Integer creator;
    @Column(name = "type")//类型
    private String type;
    @Column(name = "creattime", insertable = false, updatable = false)
    private Date creattime;
    @Column(name = "moditime", insertable = false, updatable = false)
    private Date modiTime;


    @ManyToOne
    @JoinColumn(name="creator", insertable=false, updatable=false)
    private User creatorInfo;
}

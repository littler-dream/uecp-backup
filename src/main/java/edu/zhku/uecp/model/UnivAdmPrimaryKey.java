package edu.zhku.uecp.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class UnivAdmPrimaryKey implements Serializable {
    private Integer userId;
    private Integer univId;
}

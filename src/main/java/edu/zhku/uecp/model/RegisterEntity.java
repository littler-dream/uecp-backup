package edu.zhku.uecp.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "RegisterEntity", description = "管理员注册模型")
public class RegisterEntity {
    @ApiModelProperty(value = "用户名", name = "username", example = "aabbcc", required = true)
    private String username;
    @ApiModelProperty(value = "密码", name = "username", example = "aabbcc", required = true)
    private String password;
    @ApiModelProperty(value = "授权码", name = "code", example = "123456789", required = true)
    private String code;
}

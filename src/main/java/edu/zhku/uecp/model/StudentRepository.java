package edu.zhku.uecp.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {

    @Query("select stu from Student stu where stu.usrInfo.logName=?1")
    Student findByLogname(String logname);

}

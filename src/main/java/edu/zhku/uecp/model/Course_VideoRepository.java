package edu.zhku.uecp.model;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.sql.ResultSet;
import java.util.List;

@Repository
public interface Course_VideoRepository  extends JpaRepository<Course_Video, Integer> {

    @Query(name = "select cv from Course_Video cv where cv.course_id = ?1" ,nativeQuery = true)
    List<Course_Video> findVideoByCourseId(Integer id);

}
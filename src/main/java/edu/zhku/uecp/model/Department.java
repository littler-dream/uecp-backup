package edu.zhku.uecp.model;

import lombok.Data;

import javax.persistence.*;
import java.sql.Date;

@Data
@Entity
@Table(name = "dept")
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "univid")
    private Integer univId;
    //以下两属性只读，其值由表触发器修改
    @Column(name = "creattime", insertable = false, updatable = false)
    private Date createTime;
    @Column(name = "moditime", insertable = false, updatable = false)
    private Date modiTime;

    @ManyToOne
    @JoinColumn(name="univid", insertable=false, updatable=false)
    private Universary univInfo;

}
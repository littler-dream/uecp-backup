package edu.zhku.uecp.model;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UnivAdmRepository extends JpaRepository<UnivAdm, UnivAdmPrimaryKey> {
    @Query("select ua.univInfo from UnivAdm ua where ua.userId=?1")
    List<Universary> findUnivsByAdmId(Integer usrId);
    @Query("select ua.univInfo from UnivAdm ua where ua.userId=?1 and ua.univId=?2")
    Universary findUnivByAdmId(Integer usrId, Integer univId);
    @Query("select ua.univInfo from UnivAdm ua where ua.userId=?1")
    Page<Universary> findUnivsByAdmId(Integer usrId, Pageable pageable);


}

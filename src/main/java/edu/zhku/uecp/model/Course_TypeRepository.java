package edu.zhku.uecp.model;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

@Repository
public interface Course_TypeRepository extends JpaRepository<Course_Type, Integer> {

    @Query("SELECT cv FROM Course_Type cv")
    Page<Course_Type> findAll(Pageable pageable);

    @Query("SELECT cv FROM Course_Type cv WHERE cv.type LIKE ?1")
    List<Course_Type> findByName(String name);
}
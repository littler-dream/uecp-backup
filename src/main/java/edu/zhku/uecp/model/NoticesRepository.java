package edu.zhku.uecp.model;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface NoticesRepository extends JpaRepository<Notices, Integer> {
    @Query("select n from Notices n")
    Page<Notices> findAll(Pageable pageable);

    @Query("select n from Notices n where n.id=?1")
    Notices findNoticesById(Integer id);

    @Query("select n from Notices n where n.content like CONCAT('%',:content,'%')")
    Page<Notices> findNoticesByName(@Param("content") String content, Pageable pageable);

    @Query("select n from Notices n where n.content like CONCAT('%',:content,'%')")
    List<Notices> findNoticesByName(@Param("content") String content);

}

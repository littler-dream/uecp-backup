package edu.zhku.uecp.model;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface CourseRepository extends JpaRepository<Course, Integer> {

    @Query("select c from Course c ")
    Page<Course> findAll(Pageable pageable);

    @Query("select c from Course c where c.id=?1")
    Course findCourseById(Integer id);

    @Query("select c from Course c where c.name like ?1 or c.teacher like ?1")
    Page<Course> findCourseMsgByName(String name, Pageable pageable);

    @Query("select distinct c.level from Course c")
    List<String> findCourseLevel();

    @Modifying
    @Query("UPDATE Course c SET c.video_num = (SELECT COUNT(cv) FROM Course_Video cv WHERE cv.courseId = ?1) WHERE c.id = ?1")
    void updateVideoNum(Integer id);

    @Query("SELECT c FROM Course c WHERE org_id LIKE ?1")
    List<Course> findCourseByOrgId(String org_id);
}
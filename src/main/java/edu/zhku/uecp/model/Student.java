package edu.zhku.uecp.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "stu")
public class Student {
    @Id
    @Column(name = "stuid")
    private Integer studentId;
    @Column(name = "stunum")
    private String studentNum;
    @Column(name = "classid")
    private Integer classId;

    @ManyToOne
    @JoinColumn(name="stuid", insertable=false, updatable=false)
    private User usrInfo;//基本用户信息，来自用户表*/
    @ManyToOne
    @JoinColumn(name="classid", insertable=false, updatable=false)
    private User classInfo;//基本用户信息，来自用户表*/
}

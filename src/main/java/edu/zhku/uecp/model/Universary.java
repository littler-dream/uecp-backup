package edu.zhku.uecp.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Date;

@Data
@Entity
@Table(name = "univ")
public class Universary {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "school_id")//学校标识码
    @NotNull
    private String schoolId;

    @Column(name = "school_name")
    @NotNull
    private String schoolName;

    @Column(name = "city_id")
    private Integer cityId;
    @Column(name = "province_name")
    private String provinceName;
    @Column(name = "city_name")
    private String cityName;
    @Column(name = "level")
    private String level;
    @Column(name = "department")
    private String department;
    @Column(name = "other")
    private String other;

    //以下两属性只读，其值由表触发器修改
    @Column(name = "creattime", insertable = false, updatable = false)
    private Date CreateTime;
    @Column(name = "moditime", insertable = false, updatable = false)
    private Date ModiTime;

    @ManyToOne
    @JoinColumn(name="city_id", insertable=false, updatable=false)
    private Region CityInfo;
}

package edu.zhku.uecp.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "course")
@ApiModel(value = "Course" , description = "课程实体类")
public class Course {
    @Id
    @Column(name = "id")
    @ApiModelProperty(value = "id",name = "id",hidden = true)
    private Integer id;

    @Column(name = "name")
    @NotNull
    @ApiModelProperty(value = "课程名称", name = "name", example = "计算机原理及应用", required = true)
    private String name;

    @Column(name = "introduction")
    @ApiModelProperty(value = "课程介绍", name = "introduction", example = "这是一个......的课程")
    private String introduction;

    @Column(name = "type")
    @NotNull
    @ApiModelProperty(value = "课程类别",name = "type", required = true)
    private Integer type;

    @ManyToOne
    @JoinColumn(name="type", insertable=false, updatable=false)
    private Course_Type typeInfo;

    @Column(name = "teacher")//老师
    @NotNull
    @ApiModelProperty(value = "老师",name = "teacher",example = "XXX", required = true)
    private String teacher;

    @Column(name = "org_id")
    @NotNull
    @ApiModelProperty(value = "课程机构的id",name = "org_id",example = "1111", required = true)
    private String org_id;

    @Column(name = "cover")
    @ApiModelProperty(value = "课程封面的url地址",name = "cover")
    private String cover;

    @Column(name = "video_num")
    @ApiModelProperty(value = "课程机构的视频数量",name = "video_num" ,hidden = true)
    private Integer video_num;

    @Column(name = "homework_num")
    @ApiModelProperty(value = "课程机构的作业数量",name = "homework_num" ,hidden = true)
    private Integer homework_num;

    @Column(name = "score")
    @ApiModelProperty(value = "课程评分",name = "score")
    private float score;

    @Column(name = "level")
    @ApiModelProperty(value = "课程等级",name = "level")
    private String level;
}
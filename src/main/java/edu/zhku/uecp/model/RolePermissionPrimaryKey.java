package edu.zhku.uecp.model;

import lombok.Data;
import java.io.Serializable;

@Data
public class RolePermissionPrimaryKey implements Serializable {
    private Integer roleId;
    private Integer permissionId;
}

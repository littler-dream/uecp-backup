package edu.zhku.uecp.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.SneakyThrows;
import org.apache.commons.codec.binary.Base64;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.Arrays;

//使用JPA注解配置映射关系
//@Data   //由lombok提供，自动生成get、set等方法
@Entity //告诉JPA这是一个实体类（和数据表映射的类）
@Table(name = "user") //@Table来指定和哪个数据表对应;如果省略默认表名就是User；
@ApiModel(value = "User" , description = "用户实体类")
public class User {

    @Id //这是一个主键
    @GeneratedValue(strategy = GenerationType.IDENTITY)//自增主键，由数据库生成
    @Column(name = "id")                            //省略name默认列名就是属性名
    @ApiModelProperty(value = "id",name = "id" ,hidden = true)
    private Integer id;

    @NotNull
    @Column(name = "logname",length = 20)   // 登录名，根据JPA命名规范，如name = "LogName"，
                                            // 则数据库操作时列名转换为"log_name"，
                                            // 为避免因名字转换导致运行错误，建议数据表中所有列名均小写
    @ApiModelProperty(value = "用户名（账号）", name = "logName", example = "test", required = true)
    private String logName = "usr_";        //建议默认为usr_UserID
    @Column(name = "name",length = 10)      //用户实名
    @ApiModelProperty(value = "姓名", name = "name", example = "张三", required = true)
    private String name;
    @Column(name = "sex")
    @ApiModelProperty(value = "性别", name = "sex", example = "男", required = true)
    private String sex;
    //Role-由二进制角色位(D1~D8)构成，未认证用户角色为未定Role=0。角色位为：
//    Role=0    未定(默认)
//    Role的D1=1    学生位
//            Role的D2=1    高校教师位
//            Role的D3=1    企业教师位
//    Role的D4~D7    (预留)
//    Role的D8位  系统管理员位
//    默认系统管理员兼具(D8~D1)，可随意切换角色；
//    用户在不同角色间切换后，其主界面(工作台)也应变换。
    @Column(name = "role")
    @ApiModelProperty(value = "角色（默认为游客即值为0）", name = "role", example = "0")
    private Integer role = 0;
    @NotNull
    @Column(name = "password", length = 10)
    @ApiModelProperty(value = "密码", name = "password", example = "123456", required = true)
    private String password;
    @Column(name = "mobilephone",length = 11)
    @ApiModelProperty(value = "手机号", name = "mobilePhone", example = "13428241950", required = true)
    private String mobilePhone;
    @Column(name = "email")
    @ApiModelProperty(value = "邮箱", name = "email", example = "15652455@qq.com")
    private String email;
    @Column(name = "status")
    @ApiModelProperty(value = "账号状态（0，正常；1，禁用；2，自身注销；3，普通删除）", name = "status")
    private Integer status = 0;     //默认正常
    @Column(name = "verified")
    @ApiModelProperty(value = "认证状态（0，未认证；1，已认证）", name = "verified")
    private Integer verified = 0;
    @Column(name = "verifier")
    @ApiModelProperty(value = "认证人的id", name = "verifier")
    private Integer verifier = 0;     //默认值应为系统管理员的ID
    @Column(name = "tokensecretkey")    //token加密密钥
    @ApiModelProperty(name = "tokenSecretKey" ,hidden = true)
    private String tokenSecretKey = "ZCEQIUBFKSJBFJH2020BQWE";
    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "headimage", columnDefinition="blob", nullable=true)         //用户头像
    @ApiModelProperty(value = "头像",name = "headImage" )
    private byte[] headImage;

    @ManyToOne
    @JoinColumn(name="role", insertable=false, updatable=false)
    @ApiModelProperty(name = "roleInfo",hidden = true)
    private Role roleInfo;

    public Role getRoleInfo() {
        return roleInfo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogName() {
        return logName;
    }

    public void setLogName(String logName) {
        this.logName = logName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getVerified() {
        return verified;
    }

    public void setVerified(Integer verified) {
        this.verified = verified;
    }

    public Integer getVerifier() {
        return verifier;
    }

    public void setVerifier(Integer verifier) {
        this.verifier = verifier;
    }

    public String getTokenSecretKey() {
        return tokenSecretKey;
    }

    public void setTokenSecretKey(String tokenSecretKey) {
        this.tokenSecretKey = tokenSecretKey;
    }

    public void setHeadImage(byte[] headImage) {
        this.headImage = headImage;
    }

    public String getHeadImage() throws SQLException {
        if(headImage == null)
            return null;
        return Base64.encodeBase64String(headImage);
    }

    @SneakyThrows
    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", logName='" + logName + '\'' +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", role=" + role +
                ", password='" + password + '\'' +
                ", mobilePhone='" + mobilePhone + '\'' +
                ", email='" + email + '\'' +
                ", status=" + status +
                ", verified=" + verified +
                ", verifier=" + verifier +
                ", tokenSecretKey='" + tokenSecretKey + '\'' +
                ", headImage=" + getHeadImage() +
                ", roleInfo=" + roleInfo +
                '}';
    }
}

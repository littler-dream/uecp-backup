package edu.zhku.uecp.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "role_permis")
@IdClass(RolePermissionPrimaryKey.class)   //组合主键
public class RolePermission {
    @Id
    @Column(name = "role_id", nullable = false)
    private Integer roleId;
    @Id
    @Column(name = "permis_id", nullable = false)
    private Integer permissionId;

    @ManyToOne
    @JoinColumn(name="role_id", insertable=false, updatable=false)
    private Role roleInfo;

    @ManyToOne
    @JoinColumn(name="permis_id", insertable=false, updatable=false)
    private Permission permissionInfo;
}

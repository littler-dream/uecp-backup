package edu.zhku.uecp.model;

import org.apache.shiro.authc.AuthenticationToken;

public class JWTToken implements AuthenticationToken {

    // 密钥
    private String token = null;

    public JWTToken(String token) {
        this.token = token;
    }

    @Override
    public Object getPrincipal() {

        if(token == null)
            return null;
        return token;
    }

    @Override
    public Object getCredentials() {
        if(token == null)
            return null;
        return token;
    }
}

package edu.zhku.uecp.model;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

@Data
public class UnivExcel {
    @ExcelProperty(value = "校名")
    private String name;    //变量名首字母不能大写？！
//    @ExcelProperty(value = "地址")
//    //@ExcelProperty(value = { "address" }, index = 1)
//    private String addr;
    @ExcelProperty(value = "城市(县)")
    //@ExcelProperty(value = { "city" }, index = 2)
    private String city_name;
    @ExcelProperty(value = "省份(省级市)")
    //@ExcelProperty(value = { "province" }, index = 3)
    private String province_name;
    @ExcelProperty(value = "统一社会信用代码")
    //@ExcelProperty(value = { "uscc" }, index = 4)
    private String uscc;
}

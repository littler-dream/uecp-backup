package edu.zhku.uecp.model;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

@Data
public class UserExcel {
    @ExcelProperty(value="用户姓名")
    private String name;
    @ExcelProperty(value="用户名")
    private String logName;
    @ExcelProperty(value = "密码")
    private String password;
    @ExcelProperty(value = "性别")
    private String sex;
    @ExcelProperty(value = "手机号码")
    private String mobilePhone;
    @ExcelProperty(value = "邮箱")
    private String email;
}

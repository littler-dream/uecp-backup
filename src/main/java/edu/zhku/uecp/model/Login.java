package edu.zhku.uecp.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "Login" , description = "登录模型")
public class Login {
    @ApiModelProperty(value = "用户名", name = "username", example = "aabbcc", required = true)
    private String username;
    @ApiModelProperty(value = "密码", name = "username", example = "aabbcc", required = true)
    private String password;
    @ApiModelProperty(value = "验证码", name = "captcha", example = "abcd", required = true)
    private String captcha;
}

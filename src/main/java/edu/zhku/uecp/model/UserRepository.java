package edu.zhku.uecp.model;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    @Query("select u from User u where u.logName=?1")
        //此处表名(User)和列名(LogName)应均为对应@Entity和@Column注解的类和属性名
    User findByLogname(String logname);                 //不用List<User>，LogName是唯一的

    @Query("select u from User u where u.logName=?1 and u.role>=128 ")
        //此处表名(User)和列名(LogName)应均为对应@Entity和@Column注解的类和属性名
    User findAdmByLogname(String logname);                 //不用List<User>，LogName是唯一的

    @Query("select u from User u where u.role<=4 and u.role=?1")
    List<User> findByComRole(Integer role);

    @Query("select u from User u where u.role<=4 and u.role=?1")
    Page<User> findByComRole(Integer role, Pageable pageable);

    @Query("select u from User u where u.role<=4 and u.logName like CONCAT('%',:logname,'%')")
    List<User> findByLogName(@Param("logname") String logname);

    @Query("select u from User u where u.role<=4 and u.logName like CONCAT('%',:logname,'%')")
    Page<User> findByLogName(@Param("logname") String logname, Pageable pageable);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update User u set u.headImage=?2 where u.id=?1")
    void updateHeadImage(Integer id, byte[] headImage);

    @Query("select u from User u where u.role<=4")
    List<User> findAllByRoleBefore();

    @Query("select u from User u where u.role<=4")
    Page<User> findAllByRoleBefore(Pageable pageable);

    @Query("select u from User u where u.role<=4 and u.id=?1")
    User existsByIdAndRole(Integer id);

    /**
     * 以下是管理员用户操作
     **/
    @Query("select u from User u where u.role>4")
    List<User> findAllAdmUsers();

    @Query("select u from User u where u.role>4")
    Page<User> findAllAdmUsers(Pageable pageable);

    @Query("select u from User u where u.role>4 and u.id=?1")
    User existsByAdmUsers(Integer id);

    @Query("select u from User u where u.role>=128 and u.id=?1")
    User existsSuperAdm(Integer id);
}

package edu.zhku.uecp.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "course_type")
@ApiModel(value = "Course_type" , description = "课程类型实体类")
public class Course_Type {

    @Id
    @Column(name = "id")
    @ApiModelProperty(value = "课程类型id",name = "id",hidden = true)
    private Integer id;

    @Column(name = "type")
    @NotNull
    @ApiModelProperty(value = "课程类型", name = "type", example = "通识必修类", required = true)
    private String type;
}

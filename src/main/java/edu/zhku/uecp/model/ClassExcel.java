package edu.zhku.uecp.model;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

@Data
public class ClassExcel {
    @ExcelProperty("班级")
    private String name;
    @ExcelProperty("学院")
    private String dept;
}

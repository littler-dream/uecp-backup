package edu.zhku.uecp.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

@Data   //由lombok提供，自动生成get、set等方法
@Entity //告诉JPA这是一个实体类（和数据表映射的类）
@Table(name = "role")
public class Role {
    @javax.persistence.Id
    @Column(name = "identity")
    @ApiModelProperty(value = "角色唯一标识", name = "identity", example = "64", required = true)
    private Integer identity;

    @Column(name = "label")
    @ApiModelProperty(value = "角色中文名称", name = "label", example = "管理员", required = true)
    private String label;

    @Column(name = "role_name")
    @ApiModelProperty(value = "角色字符串名称", name = "roleName", example = "adm", required = true)
    private String roleName;

    @Column(name = "state")
    @ApiModelProperty(value = "角色状态值（1，正常；0，停用）", name = "roleName", example = "1", required = true)
    private Integer state = 1;//1，正常；0，停用

    @ManyToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY)
    @JoinTable(name = "role_permis",joinColumns = @JoinColumn(name = "role_id"),
            inverseJoinColumns = @JoinColumn(name = "permis_id"))
    @ApiModelProperty(value = "权限数组", name = "permissionList")
    private List<Permission> permissionList;
}

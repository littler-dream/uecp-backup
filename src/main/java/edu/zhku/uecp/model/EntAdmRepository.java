package edu.zhku.uecp.model;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface EntAdmRepository extends JpaRepository<EntAdm, EntAdmPrimaryKey> {
    @Query("select ea.entInfo from EntAdm ea where ea.userId=?1")
    List<Enterprise> findEntsByAdmId(Integer usrId);
    @Query("select ea.entInfo from EntAdm ea where ea.userId=?1")
    Page<Enterprise> findEntsByAdmId(Integer usrId, Pageable pageable);


    @Query("select e from Enterprise e where e.name like CONCAT('%',:name,'%') and e.id=any( select ea.entId from EntAdm ea where ea.userId=:userid) ")
    Page<Enterprise> findEntsByEntName(@Param("userid") Integer id, @Param("name") String name, Pageable pageable);
    @Query("select e from Enterprise e where e.name like CONCAT('%',:name,'%')" )
    Page<Enterprise> findEntsByEntName( @Param("name") String name, Pageable pageable);

    @Query("select e from Enterprise e where e.name like CONCAT('%',:name,'%') and e.id=any( select ea.entId from EntAdm ea where ea.userId=:userid) ")
    List<Enterprise> findEntsByEntName(@Param("userid") Integer id, @Param("name") String name);
    @Query("select e from Enterprise e where e.name like CONCAT('%',:name,'%')" )
    List<Enterprise> findEntsByEntName( @Param("name") String name);


    @Query("select e from Enterprise e where e.city=:cityId")
    Page<Enterprise> findEntsByCityId(@Param("cityId") Integer cityId, Pageable pageable);
    @Query("select e from Enterprise e where e.city=:cityId and e.id=any( select ea.entId from EntAdm ea where ea.userId=:userid)")
    Page<Enterprise> findEntsByCityId(@Param("userid") Integer id,@Param("city") Integer cityId, Pageable pageable);

    @Query("select e from Enterprise e where e.city=:cityId")
    List<Enterprise> findEntsByCityId(@Param("cityId") Integer cityId);
    @Query("select e from Enterprise e where e.city=:cityId and e.id=any( select ea.entId from EntAdm ea where ea.userId=:userid)")
    List<Enterprise> findEntsByCityId(@Param("userid") Integer id,@Param("city") Integer cityId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update Enterprise e set e.name=?2 where e.id=?1")
    void update(Integer id, String name);
}
package edu.zhku.uecp.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Date;

@Data
@Entity
@Table(name = "notices")
@ApiModel(value = "Notices", description = "公告实体类")
public class Notices {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "content")
    @ApiModelProperty(value = "优先级", name = "content", example = "文案")
    private String content;

    @Column(name = "picture")
    @NotNull
    @ApiModelProperty(value = "picture的url地址", name = "picture", required = true)
    private String picture;

    @Column(name = "picture_name")
    @ApiModelProperty(value = "图片名称", name = "name", required = true)
    private String picture_name;

    @Column(name = "priority")
    @ApiModelProperty(value = "优先级", name = "priority", example = "1")
    private Integer priority;

    //以下两属性只读，其值由表触发器修改
    @Column(name = "publish_time", insertable = false, updatable = false)
    private Date publish_time;
    @Column(name = "delete_time", insertable = false, updatable = false)
    private Date delete_time;
}

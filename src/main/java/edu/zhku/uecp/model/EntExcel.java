package edu.zhku.uecp.model;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

@Data
public class EntExcel {
    @ExcelProperty(value="企业名")
    private String name;
    @ExcelProperty(value="企业简称")
    private String sname;
    @ExcelProperty(value = "地址")
    private String address;
    @ExcelProperty(value = "城市(县)")
    private String city;
    @ExcelProperty(value = "省份(省级市)")
    private String prov;
    @ExcelProperty(value = "统一社会信用代码")
    private String uscc;
}

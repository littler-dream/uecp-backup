package edu.zhku.uecp.controller.orgs;

import com.alibaba.fastjson.JSONObject;
import edu.zhku.uecp.model.Class;
import edu.zhku.uecp.model.User;
import edu.zhku.uecp.service.ClassService;
import edu.zhku.uecp.service.TokenService;
import edu.zhku.uecp.service.UnivService;
import edu.zhku.uecp.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

@Api(tags = "与院系(机构)管理相关的API接口")  //(swagger)API接口类文档
@Controller
public class ClassController {
    @Autowired
    private UserService userService;
    @Autowired
    private ClassService classService;
    @Autowired
    private TokenService tokenService;
    @Autowired
    private UnivService univService;


    @ApiOperation(value = "获取所有的院系信息，分页, 根据名称升序（只允许高校管理员）", notes = "页数从1开始")
    @GetMapping(value = "/searchDeptsByUnivId/{pageNum}/{pageSize}")
    @ApiImplicitParams({
            @ApiImplicitParam(name="Authorization",value="用户校验码token", dataType = "String",dataTypeClass = String.class,required = true),
            @ApiImplicitParam(name="userId",value="用户Id",dataType = "Integer",dataTypeClass = Integer.class, required = true),
            @ApiImplicitParam(name="univId",value="下拉框回传高校的编号",dataType = "Integer",dataTypeClass = Integer.class, required = true),
            @ApiImplicitParam(name="pageNum",value="页码",dataType = "Integer",dataTypeClass = Integer.class, required = true),
            @ApiImplicitParam(name="pageSize",value="一页的个数",dataType = "Integer",dataTypeClass = Integer.class, required = true)
    })
    public JSONObject searchDeptsByUnivId(@RequestHeader("Authorization") String strToken, @RequestHeader("userId") Integer uid,
                                          @RequestParam Integer univId, @PathVariable Integer pageNum,
                                          @PathVariable Integer pageSize){
        JSONObject json = new JSONObject();//用于输出res data
        User usr = userService.getVeriUnivAdm(uid);
        if(strToken != null && usr != null) {
            if (tokenService.verify(strToken, usr.getId(), usr.getTokenSecretKey()) != 0) {
                json.put("ret", 1);
                json.put("msg", "token签名校验失败！");
                return json;
            }
            if (univId != null) {
                if(pageNum != null && pageSize != null) {
                    Map<String, Object> orgs = univService.searchDeptsByUnivId(univId, pageNum - 1, pageSize);
                    json.put("ret", 0);
                    json.put("msg", "成功找到高校ID为" + univId + "的所有院系！");
                    json.put("orgs", orgs);
                    return json;
                } else {
                    json.put("ret", 4);
                    json.put("msg", "缺少页数或者一页的个数！");
                }
            }else{
                json.put("ret", 3);
                json.put("msg", "输入查询的名字为空");
                return json;
            }
        }
        json.put("ret", 2);
        json.put("msg", "缺少用户ID或token！");
        return json;
    }
    @ApiOperation("批量导入班级信息，只有系统管理员和院系管理员可执行该操作!")
    @PostMapping("/batchImportClasses")
    @ApiImplicitParams({
            @ApiImplicitParam(name="Authorization",value="用户校验码token", dataType = "String",dataTypeClass = String.class,required = true),
            @ApiImplicitParam(name="userId",value="用户Id", dataType = "Integer",dataTypeClass = Integer.class,required = true),
//            @ApiImplicitParam(name="file",value="Excel文件",dataType="MultipartFile", required = true),
    })
    public JSONObject batImpClasses(@RequestPart("file") MultipartFile file, @RequestHeader("userId") Integer uid,
                                    @RequestHeader("Authorization") String strToken) throws IOException {
        JSONObject json = new JSONObject();
        User usr = userService.getVeriUnivAdm(uid);
        if (usr != null &&
                tokenService.verify(strToken, uid, usr.getTokenSecretKey())==0) {
            if (file != null) {
                int count = classService.saveBatchClasses(file);
                json.put("ret", 0);
                json.put("msg", "成功上传文件！新保存" + count + "条记录！");

            } else {
                json.put("ret", 1);
                json.put("msg", "上传文件为空！");
                return json;
            }
        }
        else {
            json.put("ret", 2);
            json.put("msg", "不允许该操作！");
        }
        return json;

    }

    @ApiOperation("通过Id删除班级")
    @DeleteMapping("/{deleteClassById}")
    @ApiImplicitParams({
            @ApiImplicitParam(name="Authorization",value="用户校验码token",  dataType = "String",dataTypeClass = String.class,required = true),
            @ApiImplicitParam(name="userId",value="用户Id",  dataType = "Integer",dataTypeClass = Integer.class,required = true),
            @ApiImplicitParam(name="classId",value="要删除的班级id", dataType = "Integer",dataTypeClass = Integer.class, required = true),
    })
    public JSONObject delEntById(@RequestHeader("userId") Integer uid, @PathVariable("classId") Integer classId,
                                 @RequestHeader("token") String strToken){
        JSONObject json=new JSONObject();
        User usr = userService.getVeriUnivAdm(uid);
        if (usr != null && tokenService.verify(strToken, uid, usr.getTokenSecretKey())==0){
            if(classId != null && classService.isExist(classId)) {
                classService.delClass(classId);
                json.put("ret", 0);
                json.put("msg", "成功删除Id为" + classId + "的班级！");
            }else {
                json.put("ret", 1);
                json.put("msg", "找不到该班级！");
                return json;
            }
        }
        else {
            json.put("ret", 2);
            json.put("msg", "不允许该操作！");
        }
        return json;
    }

    @ApiOperation("通过Id更新班级名")
    @PutMapping("/updateClassById")
    @ApiImplicitParams({
            @ApiImplicitParam(name="Authorization",value="用户校验码token", dataType = "String",dataTypeClass = String.class,required = true),
            @ApiImplicitParam(name="userId",value="用户Id", dataType = "Integer",dataTypeClass = Integer.class,required = true),
            @ApiImplicitParam(name="classId",value="要更新的班级id", dataType = "Integer",dataTypeClass = Integer.class,required = true),
    })
    public JSONObject upEntById(@RequestHeader("userId") Integer uid, @RequestHeader("classId") Integer classId,
                                @RequestHeader("Authorization") String strToken,@RequestBody Class cla){
        JSONObject json = new JSONObject();
        String befName;
        User usr = userService.getVeriUnivAdm(uid);
        if (usr != null && tokenService.verify(strToken, uid, usr.getTokenSecretKey())==0){
            if(classId != null && classService.isExist(classId)) {
                befName = classService.upCla(classId, cla);
                json.put("ret", 0);
                json.put("msg", "更改班级" + befName + "成功");
            } else {
                json.put("ret", 1);
                json.put("msg", "找不到该高校！");
                return json;
            }
        } else {
            json.put("ret", 2);
            json.put("msg", "不允许该操作！");
        }
        return json;
    }

    @ApiOperation("添加单个班级")
    @PostMapping("/")
    @ApiImplicitParams({
            @ApiImplicitParam(name="Authorization",value="用户校验码token", dataType = "String",dataTypeClass = String.class,required = true),
            @ApiImplicitParam(name="userId",value="用户Id", dataType = "Integer",dataTypeClass = Integer.class,required = true)
    })
    public JSONObject addOneClass(@RequestHeader("userId") Integer uid,@RequestHeader("Authorization") String strToken,
                                @RequestBody Class cla){
        JSONObject json = new JSONObject();
        User usr = userService.getVeriUnivAdm(uid);
        if ((usr != null && tokenService.verify(strToken, uid, usr.getTokenSecretKey())==0)){
            if(cla.getName() != null && cla.getDeptId() != null) {
                classService.saveOne(cla);
                json.put("ret", 0);
                json.put("msg", "添加成功！");
            }else {
                json.put("ret", 1);
                json.put("msg", "名字为空或者deptID为空！");
                return json;
            }
        }else {
            json.put("ret", 2);
            json.put("msg", "不允许该操作！");
        }
        return json;
    }
}

package edu.zhku.uecp.controller.orgs;

import com.alibaba.fastjson.JSONObject;
import edu.zhku.uecp.model.Enterprise;
import edu.zhku.uecp.model.User;
import edu.zhku.uecp.service.EntService;
import edu.zhku.uecp.service.TokenService;
import edu.zhku.uecp.service.UserService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

@Api(tags = "与机构(企业)管理相关的API接口")  //(swagger)API接口类文档
@RestController
@RequestMapping("/orgs/ents")
public class EntController {
    @Autowired
    private UserService userService;
    @Autowired
    private EntService entService;
    @Autowired
    private TokenService tokenService;

    @ApiOperation(value = "获取企业通过id")
    @GetMapping(value = "/{id}")
    @ApiImplicitParam(name="id",value="企业id",dataType = "Integer",dataTypeClass = Integer.class, required = true)
    public JSONObject getAllEnts(@PathVariable Integer id){
        JSONObject json = new JSONObject();
        if(id != null){
            if(entService.isExist(id)) {
                Enterprise ent;
                ent = entService.findById(id);
                json.put("ret", 0);
                json.put("msg", "成功获取得到所有企业信息！");
                json.put("ent", ent);
                return json;
            }else {
                json.put("ret", 2);
                json.put("msg", "id为"+id+"的企业不存在！");
                return json;
            }
        }else {
            json.put("ret", 1);
            json.put("msg", "缺少企业id！");
            return json;
        }

    }

    @ApiOperation(value = "获取所有的企业信息，分页, 根据名称升序", notes = "页数从1开始")
    @GetMapping(value = "/{pageNum}/{pageSize}")
    @ApiImplicitParams({
            @ApiImplicitParam(name="pageNum",value="页码",dataType = "Integer",dataTypeClass = Integer.class, required = true),
            @ApiImplicitParam(name="pageSize",value="一页的个数",dataType = "Integer",dataTypeClass = Integer.class, required = true)
    })
    public JSONObject getAllEnts(@PathVariable Integer pageNum, @PathVariable Integer pageSize){
        JSONObject json = new JSONObject();
        if(pageNum != null && pageSize != null){
            Map<String, Object> orgs = entService.findAllEnts(pageNum-1,pageSize);
            json.put("ret", 0);
            json.put("msg", "成功获取到所有企业信息！");
            json.put("orgs", orgs);
            return json;
        }else {
            json.put("ret", 1);
            json.put("msg", "缺少页数或者一页的个数！");
            return json;
        }

    }

    @ApiOperation(value = "获取所有的企业信息, 不分页")
    @GetMapping(value = "/")
    public JSONObject getAllEntsNoPage(){
        JSONObject json = new JSONObject();
        Map<String, Object> orgs = entService.findAllEntsNoPage();
        json.put("ret", 0);
        json.put("msg", "成功获取得到所有企业信息！");
        json.put("orgs", orgs);
        return json;
    }

    @ApiOperation(value = "通过企业名模糊查找用户自己管理的企业", notes = "返回一页pageSize个记录，页数从1开始，如果是系统管理员则返回查询到的所有企业")
    @GetMapping(value = "/byName/{pageNum}/{pageSize}")
    @ApiImplicitParams({
            @ApiImplicitParam(name="Authorization",value="用户校验码token", dataType = "String",dataTypeClass = String.class,required = true),
            @ApiImplicitParam(name="userId",value="用户Id",dataType = "Integer",dataTypeClass = Integer.class, required = true),
            @ApiImplicitParam(name="name",value="输入查询的企业名", dataType = "String",dataTypeClass = String.class,required = true),
            @ApiImplicitParam(name="pageNum",value="页码", dataType = "Integer",dataTypeClass = Integer.class,required = true),
            @ApiImplicitParam(name="pageSize",value="一页的个数", dataType = "Integer",dataTypeClass = Integer.class,required = true)
    })
    public JSONObject searchEntByEntName(@RequestHeader("Authorization") String strToken, @RequestHeader("userId") Integer usrid,
                                         @RequestParam String name, @PathVariable Integer pageNum,
                                         @PathVariable Integer pageSize) {
        JSONObject json = new JSONObject();//用于输出res data
        User usr = userService.getUserById(usrid);
        if(strToken != null && usr != null) {
            if (tokenService.verify(strToken, usr.getId(), usr.getTokenSecretKey()) != 0) {
                json.put("ret", 1);
                json.put("msg", "token签名校验失败！");
                return json;
            }
            if (name != null) {
                if(pageNum != null && pageSize != null){
                    Map<String, Object> orgs = entService.searchEntByEntName(usr, name, pageNum-1,pageSize);
                    json.put("ret", 0);
                    json.put("msg", "成功获取得到企业信息！");
                    json.put("orgs", orgs);
                    return json;
                }else {
                    json.put("ret", 4);
                    json.put("msg", "缺少页数或者一页的个数！");
                }
            }else{
                json.put("ret", 3);
                json.put("msg", "输入查询的名字为空！");
                return json;
            }
        }
        json.put("ret", 2);
        json.put("msg", "缺少用户ID或token！");
        return json;
    }

    @ApiOperation(value = "通过地区查找用户自己管理的企业", notes = "返回一页pageSize个记录，页数从1开始，地区编号需要调用地区的接口，如果是系统管理员则返回查询到的所有企业")
    @GetMapping("/byCity/{pageNum}/{pageSize}")
    @ApiImplicitParams({
            @ApiImplicitParam(name="Authorization",value="用户校验码token",dataType = "String",dataTypeClass = String.class, required = true),
            @ApiImplicitParam(name="userId",value="用户Id",dataType = "Integer",dataTypeClass = Integer.class, required = true),
            @ApiImplicitParam(name="cityId",value="下拉框回传地区的编号",dataType = "Integer",dataTypeClass = Integer.class, required = true),
            @ApiImplicitParam(name="pageNum",value="页码", dataType = "Integer",dataTypeClass = Integer.class,required = true),
            @ApiImplicitParam(name="pageSize",value="一页的个数", dataType = "Integer",dataTypeClass = Integer.class,required = true)
    })
    public JSONObject searchEntByCity(@RequestHeader("Authorization") String strToken, @RequestHeader("userId") Integer usrid,
                                      @RequestParam Integer cityId, @PathVariable Integer pageNum,
                                      @PathVariable Integer pageSize){
        JSONObject json = new JSONObject();//用于输出res data
        User usr = userService.getUserById(usrid);
        if(strToken != null && usr != null) {
            if (tokenService.verify(strToken, usr.getId(), usr.getTokenSecretKey()) != 0) {
                json.put("ret", 1);
                json.put("msg", "token签名校验失败！");
                return json;
            }
            if (cityId != null) {
                if(pageNum != null && pageSize != null) {
                    Map<String, Object> orgs = entService.searchEntByCityId(usr, cityId, pageNum - 1, pageSize);
                    json.put("ret", 0);
                    json.put("msg", "成功获取得到企业信息！");
                    json.put("orgs", orgs);
                    return json;
                } else {
                            json.put("ret", 4);
                            json.put("msg", "缺少页数或者一页的个数！");
                        }
            }else{
                json.put("ret", 3);
                json.put("msg", "地点为空！");
                return json;
            }
        }
        json.put("ret", 2);
        json.put("msg", "缺少用户ID或token！");
        return json;
    }

    @ApiOperation(value = "通过企业名模糊查找用户自己管理的企业，不分页版", notes = "如果是系统管理员则返回查询到的所有企业")
    @GetMapping(value = "/byName")
    @ApiImplicitParams({
            @ApiImplicitParam(name="Authorization",value="用户校验码token",dataType = "String",dataTypeClass = String.class, required = true),
            @ApiImplicitParam(name="userId",value="用户Id", dataType = "Integer",dataTypeClass = Integer.class,required = true),
            @ApiImplicitParam(name="name",value="输入查询的企业名", dataType = "String",dataTypeClass = String.class,required = true)
    })
    public JSONObject searchEntByEntNameNoPage(@RequestHeader("Authorization") String strToken, @RequestHeader("userId") Integer usrid,
                                         @RequestParam String name) {
        JSONObject json = new JSONObject();//用于输出res data
        User usr = userService.getUserById(usrid);
        if(strToken != null && usr != null) {
            if (tokenService.verify(strToken, usr.getId(), usr.getTokenSecretKey()) != 0) {
                json.put("ret", 1);
                json.put("msg", "token签名校验失败！");
                return json;
            }
            if (name != null) {
                Map<String, Object> orgs = entService.searchEntByEntNameNoPage(usr, name);
                json.put("ret", 0);
                json.put("msg", "成功获取得到企业信息！");
                json.put("orgs", orgs);
                return json;

            }else{
                json.put("ret", 3);
                json.put("msg", "输入查询的名字为空！");
                return json;
            }
        }
        json.put("ret", 2);
        json.put("msg", "缺少用户ID或token！");
        return json;
    }

    @ApiOperation(value = "通过地区查找用户自己管理的企业, 不分页版", notes = "地区编号需要调用地区的接口，如果是系统管理员则返回查询到的所有企业")
    @GetMapping("/byCity")
    @ApiImplicitParams({
            @ApiImplicitParam(name="Authorization",value="用户校验码token", dataType = "String",dataTypeClass = String.class,required = true),
            @ApiImplicitParam(name="userId",value="用户Id", dataType = "Integer",dataTypeClass = Integer.class,required = true),
            @ApiImplicitParam(name="cityId",value="下拉框回传地区的编号", dataType = "Integer",dataTypeClass = Integer.class,required = true)
    })
    public JSONObject searchEntByCityNoPage(@RequestHeader("Authorization") String strToken, @RequestHeader("userId") Integer usrid,
                                      @RequestParam Integer cityId){
        JSONObject json = new JSONObject();//用于输出res data
        User usr = userService.getUserById(usrid);
        if(strToken != null && usr != null) {
            if (tokenService.verify(strToken, usr.getId(), usr.getTokenSecretKey()) != 0) {
                json.put("ret", 1);
                json.put("msg", "token签名校验失败！");
                return json;
            }
            if (cityId != null) {
                Map<String, Object> orgs = entService.searchEntByCityIdNoPage(usr, cityId);
                json.put("ret", 0);
                json.put("msg", "成功获取得到企业信息！");
                json.put("orgs", orgs);
                return json;
            }else{
                json.put("ret", 3);
                json.put("msg", "地点为空！");
                return json;
            }
        }
        json.put("ret", 2);
        json.put("msg", "缺少用户ID或token！");
        return json;
    }

    @ApiOperation(value = "批量导入企业信息，只有系统管理员可执行该操作!" , notes = "导入需要模板Excel文件")
    @PostMapping("/excelFile/")
    @ApiImplicitParams({
            @ApiImplicitParam(name="Authorization",value="用户校验码token", dataType = "String",dataTypeClass = String.class,required = true),
            @ApiImplicitParam(name="userId",value="用户Id", dataType = "Integer",dataTypeClass = Integer.class,required = true),
//            @ApiImplicitParam(name="file",value="Excel文件",dataType="MultipartFile", required = true),
    })
    public JSONObject batImpEnts(@RequestHeader("Authorization") String strToken, @RequestHeader("userId") Integer usrid,
                                 @ApiParam(value = "Excel文件模板",required = true) @RequestPart("file") MultipartFile file) throws IOException {
        JSONObject json = new JSONObject();
        User usr = userService.getVeriSysAdm(usrid);//系统管理员
        if ((usr != null && (usr.getRole() & 0x80)>0) &&
                tokenService.verify(strToken, usrid, usr.getTokenSecretKey())==0) {
            if (file != null) {
                int count = entService.saveBatchEnts(file);
                json.put("ret", 0);
                json.put("msg", "成功上传文件！新保存" + count + "条记录！");

            } else {
                json.put("ret", 1);
                json.put("msg", "上传文件为空！");
                return json;
            }
        }
        else {
            json.put("ret", 2);
            json.put("msg", "不允许该操作！");
        }
        return json;

    }

    @ApiOperation("通过Id删除企业(管理该企业的管理员或更高级管理员)")
    @DeleteMapping("/{id}")
    @ApiImplicitParams({
            @ApiImplicitParam(name="Authorization",value="用户校验码token",  dataType = "String",dataTypeClass = String.class,required = true),
            @ApiImplicitParam(name="userId",value="用户Id",  dataType = "Integer",dataTypeClass = Integer.class,required = true),
            @ApiImplicitParam(name="id",value="要删除的企业id", dataType = "Integer",dataTypeClass = Integer.class, required = true),
    })
    public JSONObject delEntById(@RequestHeader("Authorization") String strToken, @RequestHeader("userId") Integer usrid,
                                 @PathVariable Integer id){
        JSONObject json=new JSONObject();
        User usr = userService.getVeriEntAdm(usrid,id);//管理该企业的管理员或更高级管理员
        if ((usr != null && tokenService.verify(strToken, usrid, usr.getTokenSecretKey())==0)){
            if(id != null && entService.isExist(id)) {
                entService.delEnt(id);
                json.put("ret", 0);
                json.put("msg", "成功删除Id为" + id + "的企业！");
            }else {
                json.put("ret", 1);
                json.put("msg", "找不到该企业！");
                return json;
            }
        }
        else {
            json.put("ret", 2);
            json.put("msg", "不允许该操作！");
        }
        return json;
    }

    @ApiOperation(value = "通过Id更新企业  （是该企业管理员及系统管理员能操作的）")
    @PutMapping("/{id}")
    @ApiImplicitParams({
            @ApiImplicitParam(name="Authorization",value="用户校验码token", dataType = "String",dataTypeClass = String.class,required = true),
            @ApiImplicitParam(name="userId",value="用户Id", dataType = "Integer",dataTypeClass = Integer.class,required = true),
            @ApiImplicitParam(name="id",value="要更新的企业id", dataType = "Integer",dataTypeClass = Integer.class,required = true),
    })

    public JSONObject upEntById(@RequestHeader("Authorization") String strToken, @RequestHeader("userId") Integer usrid,
                                @PathVariable Integer id,@RequestBody Enterprise ent){
        JSONObject json = new JSONObject();
        String befName;
        User usr = userService.getVeriEntAdm(usrid,id);
        if ((usr != null && tokenService.verify(strToken, usrid, usr.getTokenSecretKey())==0)){
            if(id != null && entService.isExist(id)) {
                befName = entService.upEnt(id,ent);
                json.put("ret", 0);
                json.put("msg", "更改企业‘" + befName + "’ 成功！");

            }else {
                json.put("ret", 1);
                json.put("msg", "找不到该企业！");
                return json;
            }
        }
        else {
            json.put("ret", 2);
            json.put("msg", "不允许该操作！");
        }
        return json;
    }

    @ApiOperation("添加单个企业 (系统管理员)")
    @PostMapping("/")
    @ApiImplicitParams({
            @ApiImplicitParam(name="Authorization",value="用户校验码token", dataType = "String",dataTypeClass = String.class,required = true),
            @ApiImplicitParam(name="userId",value="用户Id", dataType = "Integer",dataTypeClass = Integer.class,required = true)
    })
    public JSONObject addOneEnt(@RequestHeader("Authorization") String strToken, @RequestHeader("userId") Integer usrid,
                                @RequestBody Enterprise ent){
        JSONObject json = new JSONObject();
        User usr = userService.getVeriSysAdm(usrid);//系统管理员
        if ((usr != null && tokenService.verify(strToken, usrid, usr.getTokenSecretKey())==0)){
            if(ent.getName() != null && ent.getCity() != null) {
                entService.saveOne(ent);
                json.put("ret", 0);
                json.put("msg", "添加成功！");
            }else {
                json.put("ret", 1);
                json.put("msg", "名字为空或者城市为空！");
                return json;
            }
        }else {
            json.put("ret", 2);
            json.put("msg", "不允许该操作！");
        }
        return json;
    }


}
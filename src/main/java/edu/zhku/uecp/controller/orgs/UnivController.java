package edu.zhku.uecp.controller.orgs;

import com.alibaba.fastjson.JSONObject;
import edu.zhku.uecp.model.*;
import edu.zhku.uecp.service.TokenService;
import edu.zhku.uecp.service.UnivService;
import edu.zhku.uecp.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Api(tags = "与高校(机构)管理相关的API接口")  //(swagger)API接口类文档
@RestController
@RequestMapping("/orgs/univ")
public class UnivController {
    @Autowired
    private UserService userService;
    @Autowired
    private TokenService tokenService;
    @Autowired
    private UnivService univService;

    @ApiOperation(value = "获取所有的高校信息，分页, 根据名称升序", notes = "页数从1开始")
    @GetMapping(value = "/{pageNum}/{pageSize}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页码", dataType = "Integer", dataTypeClass = Integer.class, required = true),
            @ApiImplicitParam(name = "pageSize", value = "一页的个数", dataType = "Integer", dataTypeClass = Integer.class, required = true)
    })
    public JSONObject getAllUnivs(@PathVariable Integer pageNum, @PathVariable Integer pageSize) {
        JSONObject json = new JSONObject();
        if (pageNum != null && pageSize != null) {
            Map<String, Object> orgs = univService.findAllUnivs(pageNum - 1, pageSize);
            json.put("ret", 0);
            json.put("msg", "成功获取到所有企业信息！");
            json.put("orgs", orgs);
            return json;
        } else {
            json.put("ret", 1);
            json.put("msg", "缺少页数或者一页的个数！");
            return json;
        }
    }

    @ApiOperation(value = "获取所有的高校信息, 不分页")
    @GetMapping(value = "/")
    public JSONObject getAllUnivsNoPage() {
        JSONObject json = new JSONObject();
        Map<String, Object> orgs = univService.findAllUnivsNoPage();
        json.put("ret", 0);
        json.put("msg", "成功获取得到所有高校信息！");
        json.put("orgs", orgs);
        return json;
    }

    @ApiOperation(value = "批量导入高校信息，只有系统管理员可执行该操作!", notes = "导入需要模板Excel文件")
    @PostMapping(value = "/batchImportUnivs")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "用户校验码token", dataType = "String", dataTypeClass = String.class, required = true),
            @ApiImplicitParam(name = "userId", value = "用户Id", dataType = "Integer", dataTypeClass = Integer.class, required = true),
    })
    @ResponseBody
    public JSONObject batImpUnivs(@RequestPart("file") MultipartFile file, @RequestHeader("userId") Integer uid,
                                  @RequestHeader("Authorization") String strToken) throws IOException {
        JSONObject json = new JSONObject();
        User usr = userService.getVeriSysAdm(uid);//2021-6-20之前的该行语句是bug
        if ((usr != null && (usr.getRole() & 0x80) > 0) &&
                tokenService.verify(strToken, uid, usr.getTokenSecretKey()) == 0) {
            if (file != null) {
                List<UnivExcel> errUnivs = univService.saveBatchUnivs(file);
                json.put("ret", 0);
                if (errUnivs.size() > 0) {
                    json.put("msg", "成功上传文件！有" + errUnivs.size() + "条记录未保存！");
                    json.put("errUnivs", errUnivs);
                } else
                    json.put("msg", "成功上传文件并保存所有记录！");
            } else {
                json.put("ret", 1);
                json.put("msg", "上传文件为空！");
                return json;
            }
        } else {
            json.put("ret", 2);
            json.put("msg", "不允许该操作！");
        }
        return json;
    }

    @ApiOperation("根据高校名模糊查找高校")//解决bug，有返回页数
    @GetMapping("/searchUnivByUnivName/{pageNum}/{pageSize}")
    @ApiImplicitParams({
            @ApiImplicitParam(name="Authorization",value="用户校验码token", dataType = "String",dataTypeClass = String.class,required = true),
            @ApiImplicitParam(name="userId",value="用户Id",dataType = "Integer",dataTypeClass = Integer.class, required = true),
            @ApiImplicitParam(name="schoolname",value="输入查询的高校名", dataType = "String",dataTypeClass = String.class,required = true),
            @ApiImplicitParam(name="pageNum",value="页码", dataType = "Integer",dataTypeClass = Integer.class,required = true),
            @ApiImplicitParam(name="pageSize",value="一页的个数", dataType = "Integer",dataTypeClass = Integer.class,required = true)
    })
    public JSONObject searchUnivByUnivName(@RequestHeader("Authorization") String strToken, @RequestHeader("userId") Integer usrid,
                                           @RequestParam String schoolname, @PathVariable Integer pageNum,
                                           @PathVariable Integer pageSize){
        //获取管理员信息
        JSONObject json = new JSONObject();//用于输出res data
        User usr = userService.getVeriUnivAdm(usrid);
        if(strToken != null && usr != null) {
            if (tokenService.verify(strToken, usr.getId(), usr.getTokenSecretKey()) != 0) {
                json.put("ret", 1);
                json.put("msg", "token签名校验失败！");
                return json;
            }
            if (schoolname != null) {
                if(pageNum != null && pageSize != null) {
                    Map<String, Object> orgs = univService.searchUnivByUnivName(usr, schoolname, pageNum - 1, pageSize);
                    json.put("ret", 0);
                    json.put("msg", "成功获取高校信息！");
                    json.put("orgs", orgs);
                    return json;
                } else {
                    json.put("ret", 4);
                    json.put("msg", "缺少页数或者一页的个数！");
                }
            } else {
                json.put("ret", 3);
                json.put("msg", "输入查询的名字为空");
                return json;
            }
        }
        json.put("ret", 2);
        json.put("msg", "缺少用户ID或token！");
        return json;
    }

    @ApiOperation("根据高校名模糊查找高校（不分页版）")
    @GetMapping(value = "/searchUnivByUnivNameNoPage")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "用户校验码token", dataType = "String", dataTypeClass = String.class, required = true),
            @ApiImplicitParam(name = "userId", value = "用户Id", dataType = "Integer", dataTypeClass = Integer.class, required = true),
            @ApiImplicitParam(name = "schoolname", value = "输入查询的高校名", dataType = "String", dataTypeClass = String.class, required = true),})
    public JSONObject searchUnivByUnivNameNoPage(@RequestHeader("Authorization") String strToken, @RequestHeader("userId") Integer usrid,
                                                 @RequestParam String schoolname) {
        //获取管理员信息
        JSONObject json = new JSONObject();//用于输出res data
        User usr = userService.getVeriUnivAdm(usrid);
        if (strToken != null && usr != null) {
            if (tokenService.verify(strToken, usr.getId(), usr.getTokenSecretKey()) != 0) {
                json.put("ret", 1);
                json.put("msg", "token签名校验失败！");
                return json;
            }
            if (schoolname != null) {
                Map<String, Object> orgs = univService.searchUnivByUnivNameNoPage(usr, schoolname);
                json.put("ret", 0);
                json.put("msg", "成功获取高校信息！");
                json.put("orgs", orgs);
                return json;
            } else {
                json.put("ret", 3);
                json.put("msg", "输入查询的名字为空");
                return json;
            }
        }
        json.put("ret", 2);
        json.put("msg", "缺少用户ID或token！");
        return json;
    }

    @ApiOperation("通过地名模糊查找高校")
    @GetMapping("/searchUnivByCityName/{pageNum}/{pageSize}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "用户校验码token", dataType = "String", dataTypeClass = String.class, required = true),
            @ApiImplicitParam(name = "userId", value = "用户Id", dataType = "Integer", dataTypeClass = Integer.class, required = true),
            @ApiImplicitParam(name = "cityId", value = "下拉框回传地区的编号", dataType = "Integer", dataTypeClass = Integer.class, required = true),
            @ApiImplicitParam(name = "pageNum", value = "页码", dataType = "Integer", dataTypeClass = Integer.class, required = true),
            @ApiImplicitParam(name = "pageSize", value = "一页的个数", dataType = "Integer", dataTypeClass = Integer.class, required = true)
    })
    public JSONObject searchUnivByCityName(@RequestHeader("Authorization") String strToken, @RequestHeader("userId") Integer usrid,
                                           @RequestParam Integer cityId, @PathVariable Integer pageNum,
                                           @PathVariable Integer pageSize){
        JSONObject json = new JSONObject();//用于输出res data
        User usr = userService.getVeriUnivAdm(usrid);
        if(strToken != null && usr != null) {
            if (tokenService.verify(strToken, usr.getId(), usr.getTokenSecretKey()) != 0) {
                json.put("ret", 1);
                json.put("msg", "token签名校验失败！");
                return json;
            }
            if (cityId != null) {
                if(pageNum != null && pageSize != null) {
                    Map<String, Object> orgs = univService.searchUnivByCityId(usr, cityId, pageNum - 1, pageSize);
                    json.put("ret", 0);
                    json.put("msg", "成功获取高校信息！");
                    json.put("orgs", orgs);
                    return json;
                } else {
                    json.put("ret", 4);
                    json.put("msg", "缺少页数或者一页的个数！");
                }
            }else{
                json.put("ret", 3);
                json.put("msg", "输入查询的名字为空");
                return json;
            }
        }
        json.put("ret", 2);
        json.put("msg", "缺少用户ID或token！");
        return json;
    }

    @ApiOperation("通过地名模糊查找高校")
    @GetMapping("/searchUnivByCityNameNoPage")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "用户校验码token", dataType = "String", dataTypeClass = String.class, required = true),
            @ApiImplicitParam(name = "userId", value = "用户Id", dataType = "Integer", dataTypeClass = Integer.class, required = true),
            @ApiImplicitParam(name = "cityId", value = "下拉框回传地区的编号", dataType = "Integer", dataTypeClass = Integer.class, required = true),
    })
    public JSONObject searchUnivByCityNameNoPage(@RequestHeader("Authorization") String strToken, @RequestHeader("userId") Integer usrid,
                                                 @RequestParam Integer cityId) {
        JSONObject json = new JSONObject();//用于输出res data
        User usr = userService.getVeriUnivAdm(usrid);
        if (strToken != null && usr != null) {
            if (tokenService.verify(strToken, usr.getId(), usr.getTokenSecretKey()) != 0) {
                json.put("ret", 1);
                json.put("msg", "token签名校验失败！");
                return json;
            }
            if (cityId != null) {
                Map<String, Object> orgs = univService.searchUnivByCityIdNoPage(usr, cityId);
                json.put("ret", 0);
                json.put("msg", "成功获取高校信息！");
                json.put("orgs", orgs);
                return json;
            } else {
                json.put("ret", 3);
                json.put("msg", "输入查询的名字为空");
                return json;
            }
        }
        json.put("ret", 2);
        json.put("msg", "缺少用户ID或token！");
        return json;
    }

    @ApiOperation("添加单个高校")
    @PostMapping("/addOneUniv")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "用户校验码token", dataType = "String", dataTypeClass = String.class, required = true),
            @ApiImplicitParam(name = "userId", value = "用户Id", dataType = "Integer", dataTypeClass = Integer.class, required = true)
    })
    public JSONObject addOneUniv(@RequestHeader("userId") Integer uid, @RequestHeader("Authorization") String strToken,
                                 @RequestBody Universary univ) {
        JSONObject json = new JSONObject();
        User usr = userService.getVeriSysAdm(uid);//2021-6-20之前的该行语句是bug
        if ((usr != null && tokenService.verify(strToken, uid, usr.getTokenSecretKey()) == 0)) {
            if (univ.getSchoolName() != null && univ.getCityName() != null && univ.getLevel()!=null)  {
                univService.saveOne(univ);
                json.put("ret", 0);
                json.put("msg", "添加成功！");
            }else {
                json.put("ret", 1);
                json.put("msg", "名字为空或者城市或者等级为空！");
                return json;
            }
        }else {
            json.put("ret", 2);
            json.put("msg", "不允许该操作！");
        }
        return json;
    }

    @ApiOperation("通过Id删除高校,只允许系统管理员操作")
    @DeleteMapping("/{delUnivById}")
    @ApiImplicitParams({
            @ApiImplicitParam(name="Authorization",value="用户校验码token",  dataType = "String",dataTypeClass = String.class,required = true),
            @ApiImplicitParam(name="userId",value="用户Id",  dataType = "Integer",dataTypeClass = Integer.class,required = true),
            @ApiImplicitParam(name="univId",value="要删除的高校id", dataType = "Integer",dataTypeClass = Integer.class, required = true),
    })
    public JSONObject delUnivById(@RequestHeader("Authorization") String strToken, @RequestHeader("userId") Integer uid,
                                  @PathVariable Integer univId){
        JSONObject json=new JSONObject();
        User usr = userService.getVeriSysAdm(uid);//2021-6-20之前的该行语句是bug
        if ((usr != null && tokenService.verify(strToken, uid, usr.getTokenSecretKey()) == 0)) {
            if (univId != null) {
                univService.delUniv(univId);
                json.put("ret", 0);
                json.put("msg", "成功删除Id为" + univId + "的高校！");
            } else {
                json.put("ret", 1);
                json.put("msg", "找不到该高校！");
                return json;
            }
        } else {
            json.put("ret", 2);
            json.put("msg", "不允许该操作！");
        }
        return json;
    }

    @ApiOperation("通过Id更新高校,只允许系统管理员操作")
    @PutMapping("/{upUnivById}")
    @ApiImplicitParams({
            @ApiImplicitParam(name="Authorization",value="用户校验码token", dataType = "String",dataTypeClass = String.class,required = true),
            @ApiImplicitParam(name="userId",value="用户Id", dataType = "Integer",dataTypeClass = Integer.class,required = true),
            @ApiImplicitParam(name="univId",value="要更新的高校id", dataType = "Integer",dataTypeClass = Integer.class,required = true),
    })
    public JSONObject upUnivById(@RequestHeader("Authorization") String strToken, @RequestHeader("userId") Integer uid,
                                 @RequestParam("univId") Integer univId, @RequestBody Universary univ) {
        JSONObject json = new JSONObject();
        String befName;
        User usr = userService.getVeriSysAdm(uid);
        if ((usr != null && tokenService.verify(strToken, uid, usr.getTokenSecretKey()) == 0)) {
            if (univId != null) {
                befName = univService.upUniv(univId, univ);
                json.put("ret", 0);
                json.put("msg", "更改高校" + befName + "成功");
            } else {
                json.put("ret", 1);
                json.put("msg", "找不到该高校！");
                return json;
            }
        } else {
            json.put("ret", 2);
            json.put("msg", "不允许该操作！");
        }
        return json;
    }

    @ApiOperation(value = "获取高校通过id")
    @GetMapping(value = "/{id}")
    @ApiImplicitParam(name = "id", value = "高校id", dataType = "Integer", dataTypeClass = Integer.class, required = true)
    public JSONObject getUnivById(@PathVariable Integer id) {
        JSONObject json = new JSONObject();
        if (id != null) {
            if (univService.isExist(id)) {
                Universary univ;
                univ = univService.findById(id);
                json.put("ret", 0);
                json.put("msg", "成功获取得到该高校信息！");
                json.put("yniv", univ);
                return json;
            } else {
                json.put("ret", 2);
                json.put("msg", "id为" + id + "的高校不存在！");
                return json;
            }
        } else {
            json.put("ret", 1);
            json.put("msg", "缺少高校id！");
            return json;
        }

    }
}

package edu.zhku.uecp.controller.users;

import com.alibaba.fastjson.JSONObject;
import edu.zhku.uecp.model.*;
import edu.zhku.uecp.service.TokenService;
import edu.zhku.uecp.service.User2Service;
import edu.zhku.uecp.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Api(tags = "与用户（普通用户）管理相关的接口")
@RestController
@RequestMapping(value = "/users")
public class User2Controller {
    @Autowired
    private TokenService tokenService;
    @Autowired
    private User2Service user2Service;
    @Autowired
    private UserService userService;
    @Autowired
    private RoleRepository roleRepository;

    @ApiOperation(value = "获取所有普通用户信息，不分页")
    @GetMapping()
    @RequiresPermissions({"user:view"})
    public JSONObject getAllUsersNoPage(){
        JSONObject json = new JSONObject();
        Map<String, Object> users = user2Service.findAllUsersNoPage();
        json.put("ret", 0);
        json.put("msg", "成功获取得到所有普通用户信息！");
        json.put("users", users);
        return json;
    }

    @ApiOperation(value = "获取所有普通用户信息，分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页码", dataType = "Integer", dataTypeClass = Integer.class, required = true),
            @ApiImplicitParam(name = "pageSize", value = "一页的个数", dataType = "Integer", dataTypeClass = Integer.class, required = true)
    })
    @GetMapping(value = "/{pageNum}/{pageSize}")
    @RequiresPermissions({"user:view"})
    public JSONObject getAllUsers(@PathVariable Integer pageNum, @PathVariable Integer pageSize){
        JSONObject json = new JSONObject();
        Map<String, Object> users = user2Service.findAllUsers(pageNum-1, pageSize);
        json.put("ret", 0);
        json.put("msg", "成功获取得到所有普通用户信息！");
        json.put("users", users);
        return json;
    }

    @ApiOperation(value = "获取普通用户通过id")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "用户id", dataType = "Integer", dataTypeClass = Integer.class, required = true)
    })
    @GetMapping(value = "/id/{id}")
    @RequiresPermissions({"user:view"})
    public JSONObject getAllUserById(@PathVariable Integer id){
        JSONObject json = new JSONObject();
        if(id != null){
            if (user2Service.isExistComUser(id)) {
                User user;
                user = user2Service.findById(id);
                json.put("ret", 0);
                json.put("msg", "成功获取得到该普通用户信息！");
                json.put("user", user);
                return json;
            } else {
                json.put("ret", 2);
                json.put("msg", "id为" + id + "的普通用户不存在！");
                return json;
            }
        }else {
            json.put("ret", 1);
            json.put("msg", "缺少用户id！");
            return json;
        }
    }

    @ApiOperation(value = "获取普通用户通过身份，分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "role", value = "身份", dataType = "Integer", dataTypeClass = Integer.class, required = true),
            @ApiImplicitParam(name = "pageNum", value = "页码", dataType = "Integer", dataTypeClass = Integer.class, required = true),
            @ApiImplicitParam(name = "pageSize", value = "一页的个数", dataType = "Integer", dataTypeClass = Integer.class, required = true)

    })
    @GetMapping(value = "/role/{role}/{pageNum}/{pageSize}")
    @RequiresPermissions({"user:list"})
    public JSONObject getAllUserByRole(@PathVariable Integer role,
                                       @PathVariable Integer pageNum, @PathVariable Integer pageSize){
        JSONObject json = new JSONObject();
        Map<String, Object> users = user2Service.findByComRole(role, pageNum - 1, pageSize);
        json.put("ret", 0);
        json.put("msg", "成功获取得到该身份的普通用户信息！");
        json.put("users", users);
        return json;
    }

    @ApiOperation(value = "获取普通用户通过身份id")
    @ApiImplicitParam(name = "role", value = "身份id", dataType = "Integer", dataTypeClass = Integer.class, required = true)
    @GetMapping(value = "/role/{role}")
    @RequiresPermissions({"user:list"})
    public JSONObject getAllUserByRoleNoPage(@PathVariable Integer role){
        JSONObject json = new JSONObject();
        Map<String, Object> users = user2Service.findByComRoleNoPage(role);
        json.put("ret", 0);
        json.put("msg", "成功获取得到该身份的普通用户信息！");
        json.put("users", users);
        return json;
    }

    @ApiOperation(value = "获取所有普通用户身份", notes = "由二进制角色位(D1~D8)构成，未认证用户角色为游客Role=0。角色位为：Role=0    游客(默认)；Role的D1=1    学生位；Role的D2=1    高校教师位；Role的D3=1    企业教师位；Role的D4~D7    (预留)；Role的D8位  系统管理员位。默认系统管理员兼具(D8~D1)，可随意切换角色。用户在不同角色间切换后，其主界面(工作台)也应变换。")
    @GetMapping(value = "/role")
    public JSONObject getAllUserByRoleNoPage(){
        JSONObject json = new JSONObject();
        List<Role> roles = roleRepository.findAllByComRole();
        json.put("ret", 0);
        json.put("msg", "成功获取得到所有普通用户身份信息！");
        json.put("roles", roles);
        return json;
    }

    @ApiOperation(value = "获取普通用户通过模糊搜索用户名，不分页")
    @GetMapping(value = "/logname")
    @RequiresPermissions({"user:list"})
    public JSONObject getAllUserByLogNameNoPage(@RequestParam String logname){
        JSONObject json = new JSONObject();
        Map<String, Object> users = user2Service.findByLogNameNoPage(logname);
        json.put("ret", 0);
        json.put("msg", "成功获取得到普通用户信息！");
        json.put("users", users);
        return json;
    }

    @ApiOperation(value = "获取普通用户通过模糊搜索用户名，分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "logname", value = "模糊查找的用户名", dataType = "String", dataTypeClass = String.class, required = true),
            @ApiImplicitParam(name = "pageNum", value = "页码", dataType = "Integer", dataTypeClass = Integer.class, required = true),
            @ApiImplicitParam(name = "pageSize", value = "一页的个数", dataType = "Integer", dataTypeClass = Integer.class, required = true)
    })
    @GetMapping(value = "/logname/{pageNum}/{pageSize}")
    @RequiresPermissions({"user:list"})
    public JSONObject getAllUserByLogName(@RequestParam String logname, @PathVariable Integer pageNum, @PathVariable Integer pageSize){
        JSONObject json = new JSONObject();
        Map<String, Object> users = user2Service.findByLogName(logname, pageNum - 1, pageSize);
        json.put("ret", 0);
        json.put("msg", "成功获取得到普通用户信息！");
        json.put("users", users);
        return json;
    }

    @ApiOperation(value = "添加单个普通用户", notes = "头像最大为64KB，要不前端直接传入头像的base64编码字符串，这样就可以直接放同一个body里了")
    @PostMapping(value = "")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name="Authorization",value="用户校验码token",  dataType = "String",dataTypeClass = String.class,required = true),
//            @ApiImplicitParam(name="userId",value="用户Id",  dataType = "Integer",dataTypeClass = Integer.class,required = true)
//    })
    @RequiresPermissions({"user:add"})
    public JSONObject saveUser(@RequestBody User user) throws IOException {
        JSONObject json = new JSONObject();
//        User usr = userService.getVeriSysAdm(usrid);//系统管理员
//        if ((usr != null && tokenService.verify(strToken, usrid, usr.getTokenSecretKey())==0)){
        if (user.getRole() <= 4) {
            if (!user2Service.getUserByLogName(user.getLogName())) {
//                if(file == null){
//                    user.setHeadImage(null);
//                } else {
//                    if (file.getSize() > 65535){//65535 = 64KB
//                        json.put("ret", 3);
//                        json.put("msg", "头像图片大于64KB，上传失败！");
//                        return json;
//                    }else {
//                        user.setHeadImage(file.getBytes());
//                    }
                user2Service.saveUser(user);
                json.put("ret", 0);
                json.put("msg", "添加用户成功！");
//                }
            } else {
                json.put("ret", 1);
                json.put("msg", "该用户名已经存在！");
            }
        } else {
            json.put("ret", 2);
            json.put("msg", "添加身份错误！");
        }
//        }
//        else {
//            json.put("ret", 2);
//            json.put("msg", "不允许该操作！");
//        }
        return json;
    }

    @ApiOperation("通过Id删除普通用户")
    @DeleteMapping("/{id}")
    @ApiImplicitParams({
//            @ApiImplicitParam(name="Authorization",value="用户校验码token",  dataType = "String",dataTypeClass = String.class,required = true),
//            @ApiImplicitParam(name="userId",value="用户Id",  dataType = "Integer",dataTypeClass = Integer.class,required = true),
            @ApiImplicitParam(name = "id", value = "要删除的普通用户id", dataType = "Integer", dataTypeClass = Integer.class, required = true),
    })
    @RequiresPermissions({"user:delete"})
    public JSONObject delUserById(@PathVariable Integer id) {
        JSONObject json = new JSONObject();
//        User usr = userService.getVeriSysAdm(usrid);//系统管理员
//        if ((usr != null && tokenService.verify(strToken, usrid, usr.getTokenSecretKey())==0)){
        if (id != null && user2Service.isExistComUser(id)) {
            user2Service.delUserById(id);
            json.put("ret", 0);
            json.put("msg", "成功删除Id为" + id + "的普通用户！");
        } else {
            json.put("ret", 1);
            json.put("msg", "找不到该普通用户！");
            return json;
        }
//        }
//        else {
//            json.put("ret", 2);
//            json.put("msg", "不允许该操作！");
//        }
        return json;
    }

    @ApiOperation(value = "通过Id更新普通用户信息  （用户自己管理？）", notes = "用户名不允许更改")
    @PutMapping("/{id}")
    @ApiImplicitParams({
//            @ApiImplicitParam(name="Authorization",value="用户校验码token", dataType = "String",dataTypeClass = String.class,required = true),
//            @ApiImplicitParam(name="userId",value="用户Id", dataType = "Integer",dataTypeClass = Integer.class,required = true),
            @ApiImplicitParam(name = "id", value = "要更新的用户id", dataType = "Integer", dataTypeClass = Integer.class, required = true),
    })
    @RequiresPermissions({"user:update"})
    public JSONObject upUserById(@PathVariable Integer id, @RequestBody User user) {
        JSONObject json = new JSONObject();
        String befName;
//        if ((usr != null && tokenService.verify(strToken, usrid, usr.getTokenSecretKey())==0)){
        if (id != null && user2Service.isExistComUser(id)) {
            befName = user2Service.upUser(id, user);
            json.put("ret", 0);
            json.put("msg", "更改用户名为‘" + befName + "’ 的普通用户成功！");

        } else {
            json.put("ret", 1);
            json.put("msg", "找不到该普通用户！");
            return json;
        }
//        }
//        else {
//            json.put("ret", 2);
//            json.put("msg", "不允许该操作！");
//        }
        return json;
    }

    @ApiOperation("批量导入普通用户信息")
    @PostMapping(value = "/batchImportUsers")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name="Authorization",value="用户校验码token",  dataType = "String",dataTypeClass = String.class,required = true),
//            @ApiImplicitParam(name="userId",value="用户Id",  dataType = "Integer",dataTypeClass = Integer.class,required = true)
//    })
    @RequiresPermissions({"user:import"})
    public JSONObject batImpUsers(@RequestPart("file") MultipartFile file) throws IOException {
        JSONObject json = new JSONObject();
//        User usr = userService.getUserById(uid);
//        if (usr != null && tokenService.verify(strToken, uid, usr.getTokenSecretKey()) == 0) {
        if (file != null) {
            List<UserExcel> errUsers = user2Service.saveBatchUsers(file);
            json.put("ret", 0);
            if (errUsers.size() > 0) {
                json.put("msg", "成功上传文件！有" + errUsers.size() + "条记录未保存！");
                json.put("errUsers", errUsers);
            } else
                    json.put("msg", "成功上传文件并保存所有记录！");
            } else {
                json.put("ret", 1);
                json.put("msg", "上传文件为空！");
                return json;
            }
//        } else {
//            json.put("ret", 2);
//            json.put("msg", "不允许该操作！");
//        }
        return json;
    }

    @ApiOperation(value = "更改/上传用户头像", notes = "头像最大为64KB")
    @PostMapping(value = "/headImage")
    @ApiImplicitParams({
            @ApiImplicitParam(name="Authorization",value="用户校验码token",  dataType = "String",dataTypeClass = String.class,required = true),
            @ApiImplicitParam(name="userId",value="用户Id",  dataType = "Integer",dataTypeClass = Integer.class,required = true)
    })
    public JSONObject saveHeadImage(@RequestParam("userId") Integer uid,
                                    @RequestParam("Authorization") String strToken,@RequestPart MultipartFile file) throws IOException {
        JSONObject json = new JSONObject();
        User usr = user2Service.findById(uid);
        if (usr != null && tokenService.verify(strToken, uid, usr.getTokenSecretKey()) == 0) {
            if(file != null) {
                if (file.getSize() > 65535) {//65535 = 64KB
                    json.put("ret", 3);
                    json.put("msg", "头像图片大于64KB，上传失败！");
                    return json;
                }
                user2Service.saveHeadImage(uid,file);
                json.put("ret", 0);
                json.put("msg", "上传成功！");
            }else {
                json.put("ret", 1);
                json.put("msg", "上传头像为空！");
            }
        } else {
            json.put("ret", 2);
            json.put("msg", "不允许该操作！");
        }
        return json;
    }
}

package edu.zhku.uecp.controller.users;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.code.kaptcha.impl.DefaultKaptcha;
import edu.zhku.uecp.model.Login;
import edu.zhku.uecp.model.User;
import edu.zhku.uecp.service.TokenService;
import edu.zhku.uecp.service.UserService;
import io.swagger.annotations.*;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Blob;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Api(tags = "与用户相关的接口")
@RestController
@RequestMapping("/rest/usr")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private TokenService tokenService;
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    @Autowired
    private DefaultKaptcha kaptcha;

    @ApiOperation(value = "获取验证码", notes = "获取验证码(com.google.code.kaptcha+redis版)，并将登录的Challenge放在请求头", httpMethod = "GET")
    @GetMapping(value = "/getCaptcha")
    public void getCaptcha(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String ip = request.getRemoteAddr();

        //challenge在response head，kaptcha图片在response body
        String challenge = RandomStringUtils.randomAlphanumeric(4);
        response.setContentType("image/jpeg");
        String kaptchaText = kaptcha.createText();
        redisTemplate.opsForValue().set(ip + challenge, kaptchaText, 10, TimeUnit.MINUTES);
        response.setHeader("Challenge", challenge);
        ImageIO.write(kaptcha.createImage(kaptchaText), "jpg", response.getOutputStream());
    }

    @ApiOperation(value = "用户登录", notes = "用户登录，要求：验证码由(com.google.code.kaptcha)生成", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Challenge", value = "Challenge code for Captcha(不是验证码，在请求头的那个码)", dataType = "String",dataTypeClass = String.class,required = true,  paramType = "header")
//            @ApiImplicitParam(name = "captcha", value = "验证码", required = true, dataType = "String", paramType = "body", examples = @Example({
//                    @ExampleProperty(value = "{\"captcha\":\"captcha\"}", mediaType = "application/json")
//            })),
//            @ApiImplicitParam(name = "username", value = "用户登录名", required = true, dataType = "String", paramType = "body", examples = @Example({
//                    @ExampleProperty(value = "{\"username\":\"username\"}", mediaType = "application/json")
//            })),
//            @ApiImplicitParam(name = "password", value = "用户密码", required = true, dataType = "String", paramType = "body", examples = @Example({
//                    @ExampleProperty(value = "{\"password\":\"password\"}", mediaType = "application/json")
//            }))
    })
    @PostMapping(value = "/login")
    public void login(@RequestBody Login login,HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setCharacterEncoding("UTF-8");//已在WebMvcConfig中全局设置

        //getKaptcha()生成的验证码用下面这段代码校验
//        String strParams = new String(request.getInputStream().readAllBytes());
//        JSONObject params = JSON.parseObject(strParams);
//        String code = params.getString("captcha");
        String code = login.getCaptcha();
        String challenge = request.getHeader("Challenge");
        String trueCode = (String) redisTemplate.opsForValue().get(request.getRemoteAddr() + challenge);
        JSONObject json = new JSONObject();
        if (!code.equals(trueCode)) {
            json.put("ret", -1); //此处错误编码和错误消息暂时用硬编码，以后应设置错误常量，所有错误编码和提示全局统一定义
            json.put("msg", "验证码错误或超时");
            response.getWriter().write(json.toString());
            return;
        }
        redisTemplate.delete(request.getRemoteAddr() + challenge);

        //校验用户账号，成功则生成JWT令牌写入json的token字段返回客户端，用户以后访问服务器使用该token
        User usr;
        try {
            usr = userService.getVeriAdm(login.getUsername());
            if (usr == null) {
                json.put("ret", -2);
                json.put("msg", "用户非管理员");
            } else if (usr.getPassword().equals(login.getPassword())) {
                if (usr.getStatus() == 0) {
                    json.put("ret", 0);
                    json.put("msg", "登录成功");
                    //服务器端使用带签名的token(JWT)，而非session验证用户身份：
                    json.put("token", tokenService.sign(usr));
                    json.put("realName", usr.getLogName());
                    json.put("userId", usr.getId());
                    String headImg = usr.getHeadImage();
//                    if (headImg != null)//将头像数据用Base64编码
                    json.put("headImg", headImg);
                } else {
                    json.put("ret", -3);
                    json.put("msg", "用户状态异常，请联系管理员");
                }
            } else {
                json.put("ret", -4);
                json.put("msg", "密码错误");
            }
        } catch (Exception e) {
            e.printStackTrace();
            json.put("ret", -5);
            json.put("msg", "数据访问异常");
        }
        response.getWriter().write(json.toString());
    }

    @ApiOperation(value = "获取管理员信息", notes = "只是一个测试，在用户登录后验证JWT token签名，判断是否管理员信息，如是则返回其管理机构信息", httpMethod = "GET")
    @ResponseBody
    @GetMapping(value = "/admInfo")
    public JSONObject admInfo(@RequestHeader("UserId") Integer userId,
                              @RequestHeader("Authorization") String strToken) {
        //获取管理员信息
        JSONObject json = new JSONObject();//用于输出res data
        if (userId != null && strToken != null) {
            User usr = userService.getVeriAdm(userId);
            if (usr == null) {
                json.put("ret", 1);
                json.put("msg", "非管理员，禁止使用后台管理系统！");
                return json;
            }
            if (tokenService.verify(strToken, usr.getId(), usr.getTokenSecretKey()) != 0) {
                json.put("ret", 2);
                json.put("msg", "token校验失败，请重新登录！");
                return json;
            }
            Map<String, Object> orgs = userService.getOrgsManagedBy(usr);
            json.put("ret", 0);
            json.put("msg", "成功获取管理员信息！");
            json.put("orgs", orgs);
            return json;
        }
        json.put("ret", 3);
        json.put("msg", "缺少用户ID或token！");
        return json;
    }

    @ApiOperation(value = "获取管理员管理的三类机构(高校、企业和院系)信息", notes = "获取管理员管理的三类机构(高校、企业和院系)信息，结果分页(每页10条记录)，返回第一页的内容", httpMethod = "GET")
    @GetMapping("/getAdmOrgs")
    @ResponseBody   //返回数据在ResponseBody中
    public JSONObject getAdmOrgs(@RequestHeader("UserId") Integer userId,
                                 @RequestHeader("Authorization") String strToken,
                                 @RequestParam Integer pageNum) throws IOException {
        JSONObject json = new JSONObject();
        if (pageNum == null) pageNum=0;
        if (userId != null && strToken != null) {
            User usr = userService.getVeriAdm(userId);
            if (usr == null) {
                json.put("ret", 1);
                json.put("msg", "非管理员，禁止使用后台管理系统！");
                return json;
            }
            if (tokenService.verify(strToken, usr.getId(), usr.getTokenSecretKey()) != 0) {
                json.put("ret", 2);
                json.put("msg", "token签名校验失败！");
                return json;
            }
            Map<String, Object> orgs = userService.getPagedAndIdOrderedOrgsManagedBy(usr, pageNum);
            json.put("ret", 0);
            json.put("msg", "成功获取管理员信息！");
            json.put("orgs", orgs);
            return json;
        }
        json.put("ret", 3);
        json.put("msg", "缺少用户ID或token！");
        return json;
    }
}

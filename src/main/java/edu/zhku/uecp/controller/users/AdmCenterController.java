package edu.zhku.uecp.controller.users;

import com.alibaba.fastjson.JSONObject;
import edu.zhku.uecp.model.RegisterEntity;
import edu.zhku.uecp.model.User;
import edu.zhku.uecp.service.AdmCenterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Api(tags = "与用户（管理员用户）管理相关的接口")
@RestController
@RequestMapping("/admUsers")
public class AdmCenterController {
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    @Autowired
    private AdmCenterService admCenterService;
    private static volatile int Guid = 100;

    @ApiOperation(value = "获取所有管理员用户信息，不分页")
    @GetMapping()
//    @RequiresPermissions({"user:view"})
    public JSONObject getAllUsersNoPage() {
        JSONObject json = new JSONObject();
        Map<String, Object> users = admCenterService.findAllAdmUsersNoPage();
        json.put("ret", 0);
        json.put("msg", "成功获取得到所有管理员用户信息！");
        json.put("admUsers", users);
        return json;
    }

    @ApiOperation(value = "获取所有管理员用户信息，分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页码", dataType = "Integer", dataTypeClass = Integer.class, required = true),
            @ApiImplicitParam(name = "pageSize", value = "一页的个数", dataType = "Integer", dataTypeClass = Integer.class, required = true)
    })
    @GetMapping(value = "/{pageNum}/{pageSize}")
//    @RequiresPermissions({"user:view"})
    public JSONObject getAllUsers(@PathVariable Integer pageNum, @PathVariable Integer pageSize) {
        JSONObject json = new JSONObject();
        Map<String, Object> users = admCenterService.findAllAdmUsers(pageNum - 1, pageSize);
        json.put("ret", 0);
        json.put("msg", "成功获取得到所有管理员用户信息！");
        json.put("users", users);
        return json;
    }

    @ApiOperation(value = "获取授权码", notes = "有效时长2小时")
    @GetMapping("/getAuthorizationCode")
    @ApiImplicitParam(name = "role", value = "角色唯一标识", dataType = "Integer", dataTypeClass = Integer.class, required = true)
//    @RequiresPermissions({"user:view"})
    public void getAuthorizationCode(HttpServletRequest request, HttpServletResponse response, @RequestParam Integer role) throws IOException {
        response.setCharacterEncoding("UTF-8");
        JSONObject json = new JSONObject();
        String ip = request.getRemoteAddr();
        if (role == 128) {
            json.put("ret", 1);
            json.put("msg", "身份输入错误！");
            response.getWriter().write(json.toString());
            return;
        }
        Guid += 1;

        long now = System.currentTimeMillis();
        //获取4位年份数字
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy");
        //获取时间戳
        String time = dateFormat.format(now);
        String info = now + "";
        //获取三位随机数
        //int ran=(int) ((Math.random()*9+1)*100);
        //要是一段时间内的数据量过大会有重复的情况，所以做以下修改
        int ran = 0;
        if (Guid > 999) {
            Guid = 100;
        }
        ran = Guid;
        redisTemplate.opsForValue().set(ip + time + info.substring(2, info.length()) + ran, role, 60 * 2, TimeUnit.MINUTES);
        json.put("ret", 0);
        json.put("msg", "成功获取到授权码！");
        json.put("code", time + info.substring(2, info.length()) + ran);
        response.getWriter().write(json.toString());
    }

    @ApiOperation(value = "管理员注册")
    @PostMapping("/register")
//    @RequiresPermissions({"user:view"})
    public void register(HttpServletRequest request, HttpServletResponse response,
                         @RequestBody RegisterEntity registerEntity) throws IOException {
        response.setCharacterEncoding("UTF-8");
        JSONObject json = new JSONObject();
        Integer role = (Integer) redisTemplate.opsForValue().get(request.getRemoteAddr() + registerEntity.getCode());
        System.out.println(role);
        if (role == null) {
            json.put("ret", 1);
            json.put("msg", "授权码错误！");
            response.getWriter().write(json.toString());
            return;
        }
        User user = new User();
        user.setPassword(registerEntity.getPassword());
        user.setLogName(registerEntity.getUsername());
        user.setName("adm");
        user.setRole(role);
        user.setVerified(1);
        Integer num = admCenterService.saveAdmUser(user);
        if (num == 0) {
            json.put("ret", 0);
            json.put("msg", "注册成功！");
            redisTemplate.delete(request.getRemoteAddr() + registerEntity.getCode());
        }
        response.getWriter().write(json.toString());
    }

    @ApiOperation(value = "获取管理员用户通过id")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "管理员用户id", dataType = "Integer", dataTypeClass = Integer.class, required = true)
    })
    @GetMapping(value = "/{id}")
//    @RequiresPermissions({"user:view"})
    public JSONObject getAllUserById(@PathVariable Integer id) {
        JSONObject json = new JSONObject();
        if (id != null) {
            if (admCenterService.isExistAdmUser(id)) {
                User user;
                user = admCenterService.findById(id);
                json.put("ret", 0);
                json.put("msg", "成功获取得到该管理员用户信息！");
                json.put("admUser", user);
            } else {
                json.put("ret", 2);
                json.put("msg", "id为" + id + "的管理员用户不存在！");
            }
            return json;
        } else {
            json.put("ret", 1);
            json.put("msg", "缺少用户id！");
            return json;
        }
    }

    @ApiOperation(value = "通过Id更新管理员用户信息", notes = "用户名和权限等级不允许更改")
    @PutMapping("/common/{id}")
    @ApiImplicitParams({
//            @ApiImplicitParam(name="Authorization",value="用户校验码token", dataType = "String",dataTypeClass = String.class,required = true),
//            @ApiImplicitParam(name="userId",value="用户Id", dataType = "Integer",dataTypeClass = Integer.class,required = true),
            @ApiImplicitParam(name = "id", value = "要更新的管理员用户id", dataType = "Integer", dataTypeClass = Integer.class, required = true),
    })
//    @RequiresPermissions({"user:update"})
    public JSONObject upUserById(@PathVariable Integer id, @RequestBody User user) {
        JSONObject json = new JSONObject();
        Integer num;
//        if ((usr != null && tokenService.verify(strToken, usrid, usr.getTokenSecretKey())==0)){
        if (!admCenterService.isSuperAdm(id)) {
            if (id != null && admCenterService.isExistAdmUser(id)) {
                num = admCenterService.upAdmUser(id, user);
                if (num == 0) {
                    json.put("ret", 0);
                    json.put("msg", "更新信息成功！");
                } else {
                    json.put("ret", 3);
                    json.put("msg", "不允许更改权限等级！");
                }

            } else {
                json.put("ret", 1);
                json.put("msg", "找不到该普通用户！");
                return json;
            }
        } else {
            json.put("ret", 4);
            json.put("msg", "superAdm禁止更改！");
        }
//        }
//        else {
//            json.put("ret", 2);
//            json.put("msg", "不允许该操作！");
//        }
        return json;
    }

    @ApiOperation(value = "通过Id更新管理员用户信息(最高管理员可调用)", notes = "用户名不允许更改")
    @PutMapping("/adm/{id}")
    @ApiImplicitParams({
//            @ApiImplicitParam(name="Authorization",value="用户校验码token", dataType = "String",dataTypeClass = String.class,required = true),
//            @ApiImplicitParam(name="userId",value="用户Id", dataType = "Integer",dataTypeClass = Integer.class,required = true),
            @ApiImplicitParam(name = "id", value = "要更新的管理员用户id", dataType = "Integer", dataTypeClass = Integer.class, required = true),
    })
//    @RequiresPermissions({"user:update"})
    public JSONObject upUserBySuperAdm(@PathVariable Integer id, @RequestBody User user) {
        JSONObject json = new JSONObject();
        Integer num;
//        if ((usr != null && tokenService.verify(strToken, usrid, usr.getTokenSecretKey())==0)){
        if (id != null && admCenterService.isExistAdmUser(id)) {
            num = admCenterService.upAdmUserBySuperAdm(id, user);
            if (num == 0) {
                json.put("ret", 0);
                json.put("msg", "更新信息成功！");
            }
        } else {
            json.put("ret", 1);
            json.put("msg", "找不到该普通用户！");
            return json;
        }
//        }
//        else {
//            json.put("ret", 2);
//            json.put("msg", "不允许该操作！");
//        }
        return json;
    }

    @ApiOperation("通过Id删除管理员用户")
    @DeleteMapping("/{id}")
    @ApiImplicitParams({
//            @ApiImplicitParam(name="Authorization",value="用户校验码token",  dataType = "String",dataTypeClass = String.class,required = true),
//            @ApiImplicitParam(name="userId",value="用户Id",  dataType = "Integer",dataTypeClass = Integer.class,required = true),
            @ApiImplicitParam(name = "id", value = "要删除的管理员用户id", dataType = "Integer", dataTypeClass = Integer.class, required = true),
    })
//    @RequiresPermissions({"user:delete"})
    public JSONObject delUserById(@PathVariable Integer id) {
        JSONObject json = new JSONObject();
//        User usr = userService.getVeriSysAdm(usrid);//系统管理员
//        if ((usr != null && tokenService.verify(strToken, usrid, usr.getTokenSecretKey())==0)){
        if (!admCenterService.isSuperAdm(id)) {
            if (id != null && admCenterService.isExistAdmUser(id)) {
                Integer num = admCenterService.delAdmUser(id);
                if (num == 0) {
                    json.put("ret", 0);
                    json.put("msg", "成功删除Id为" + id + "的管理员用户！");
                }
            } else {
                json.put("ret", 1);
                json.put("msg", "找不到该管理员用户！");
                return json;
            }
        } else {
            json.put("ret", 4);
            json.put("msg", "superAdm禁止更改！");
        }
//        }
//        else {
//            json.put("ret", 2);
//            json.put("msg", "不允许该操作！");
//        }
        return json;
    }

    @ApiOperation(value = "修改管理员权限等级", notes = "管理员都可调用")
    @PutMapping("/role/{id}")
    @ApiImplicitParams({
//            @ApiImplicitParam(name="Authorization",value="用户校验码token", dataType = "String",dataTypeClass = String.class,required = true),
//            @ApiImplicitParam(name="userId",value="用户Id", dataType = "Integer",dataTypeClass = Integer.class,required = true),
            @ApiImplicitParam(name = "id", value = "要更新的管理员用户id", dataType = "Integer", dataTypeClass = Integer.class, required = true),
            @ApiImplicitParam(name = "code", value = "授权码", dataType = "String", dataTypeClass = String.class, required = true)
    })
//    @RequiresPermissions({"user:update"})
    public void upUserRole(HttpServletRequest request, HttpServletResponse response,
                           @PathVariable Integer id, @RequestParam String code) throws IOException {
        JSONObject json = new JSONObject();
        Integer num;
//        if ((usr != null && tokenService.verify(strToken, usrid, usr.getTokenSecretKey())==0)){
        if (!admCenterService.isSuperAdm(id)) {
            if (id != null && admCenterService.isExistAdmUser(id)) {
                Integer role = (Integer) redisTemplate.opsForValue().get(request.getRemoteAddr() + code);
                num = admCenterService.upUserRole(id, role);
                if (num == 0) {
                    json.put("ret", 0);
                    json.put("msg", "更新信息成功！");
                } else {
                    json.put("ret", 3);
                    json.put("msg", "不允许更改权限等级！");
                }

            } else {
                json.put("ret", 1);
                json.put("msg", "找不到该普通用户！");
            }
        } else {
            json.put("ret", 4);
            json.put("msg", "superAdm禁止更改！");
        }
//        }
//        else {
//            json.put("ret", 2);
//            json.put("msg", "不允许该操作！");
//        }
        response.getWriter().write(json.toString());
    }

}

package edu.zhku.uecp.controller;

import com.alibaba.fastjson.JSONObject;
import edu.zhku.uecp.model.Role;
import edu.zhku.uecp.service.RoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Api(tags = "与权限管理相关的接口")
@RestController
@RequestMapping(value = "/roles")
public class RoleController {
    @Autowired
    private RoleService roleService;

    @ApiOperation(value = "获取所有权限信息")
    @GetMapping("/permissions")
//    @RequiresPermissions({"permission:view"})
    public JSONObject findAllPermis(){
        JSONObject json = new JSONObject();
        json.put("ret",0);
        json.put("msg","成功获取全部权限信息");
        json.put("permissions",roleService.findAllPermis());
        return json;
    }

    @ApiOperation(value = "获取所有角色（权限）信息，不分页")
    @GetMapping()
    @RequiresPermissions({"permission:view"})
    public JSONObject findAllRoleNoPage(){
        JSONObject json = new JSONObject();
        json.put("ret",0);
        json.put("msg","成功获取全部角色（权限）信息");
        json.put("roles",roleService.findAllRoleNoPage());
        return json;
    }

    @ApiOperation(value = "获取所有角色（权限）信息，分页")
    @GetMapping("/{pageNum}/{pageSize}")
    @RequiresPermissions({"permission:view"})
    public JSONObject findAllRole(@PathVariable Integer pageNum, @PathVariable Integer pageSize) {
        JSONObject json = new JSONObject();
        json.put("ret", 0);
        json.put("msg", "成功获取全部角色（权限）信息");
        json.put("roles", roleService.findAllRole(pageNum - 1, pageSize));
        return json;
    }

    @ApiOperation(value = "获取角色（权限）信息通过identity")
    @ApiImplicitParam(name = "identity", value = "角色唯一标识", dataType = "Integer", dataTypeClass = Integer.class, required = true)
    @GetMapping("/{identity}")
    @RequiresPermissions({"permission:view"})
    public JSONObject findRoleById(@PathVariable Integer identity) {
        JSONObject json = new JSONObject();
        json.put("ret", 0);
        json.put("msg", "成功获取该角色（权限）信息");
        json.put("role", roleService.findRoleByIdentity(identity));
        return json;
    }

    @ApiOperation(value = "添加单个角色（权限）信息")
    @PostMapping()
    @RequiresRoles("superAdm")
    public JSONObject saveRole(@RequestBody Role role) {
        JSONObject json = new JSONObject();
        Role role2 = roleService.findRoleByIdentity(role.getIdentity());
        if (role2 == null) {
            roleService.addRole(role);
            json.put("ret", 0);
            json.put("msg", "添加角色（权限）成功！");
        } else {
            json.put("ret", 1);
            json.put("msg", "该角色已经存在！");
        }
        return json;
    }

    @ApiOperation(value = "删除角色（权限）信息")
    @ApiImplicitParam(name = "identity", value = "角色唯一标识", dataType = "Integer", dataTypeClass = Integer.class, required = true)
    @DeleteMapping("/{identity}")
    @RequiresRoles("superAdm")
    public JSONObject delRole(@PathVariable Integer identity) {
        JSONObject json = new JSONObject();
        if (!roleService.isSuperAdm(identity)) {
            roleService.delRole(identity);
            json.put("ret", 0);
            json.put("msg", "删除角色（权限）成功！");
        } else {
            json.put("ret", 2);
            json.put("msg", "超级管理员禁止删除！");
        }
        return json;
    }

    @ApiOperation(value = "更新角色（权限）信息")
    @ApiImplicitParam(name = "identity", value = "角色唯一标识", dataType = "Integer", dataTypeClass = Integer.class, required = true)
    @PutMapping("/{identity}")
    @RequiresRoles("superAdm")
    public JSONObject updateRole(@RequestBody Role role, @PathVariable Integer identity) {
        JSONObject json = new JSONObject();
        if (!roleService.isSuperAdm(identity)) {
            if (roleService.isEx(identity)) {
                roleService.upRole(role, identity);
                json.put("ret", 0);
                json.put("msg", "更新角色（权限）成功！");
            } else {
                json.put("ret", 1);
                json.put("msg", "角色不存在！");
            }
        } else {
            json.put("ret", 2);
            json.put("msg", "超级管理员权限禁止更改！");
        }
        return json;
    }

}

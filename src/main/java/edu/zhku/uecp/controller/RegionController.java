package edu.zhku.uecp.controller;

import com.alibaba.fastjson.JSONObject;
import edu.zhku.uecp.model.RegionRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Api(tags = "与地区相关的接口")  //(swagger)API接口类文档
@RestController
public class RegionController {
    @Autowired
    private RegionRepository regionRep;

    @ApiOperation(value = "获取所有的省份", httpMethod = "GET")
    @GetMapping("/Provs")
    public JSONObject getAllProvs() {
        JSONObject json = new JSONObject();
        json.put("ret", 0);
        json.put("msg", "成功获取所有省份信息！");
        json.put("provs", regionRep.findAllProvsOrderByPinyin());
        return json;
    }

    @ApiOperation(value = "获取单个城市（通过省名和城市名）", httpMethod = "GET")
    @GetMapping("/Cities")
    @ApiImplicitParams({
            @ApiImplicitParam(name="provName",value="省名",dataType = "String",dataTypeClass = String.class, required = true),
            @ApiImplicitParam(name="cityName",value="城市名",dataType = "String",dataTypeClass = String.class, required = true)
    })
    public JSONObject getCity(@RequestParam String provName, @RequestParam String cityName) {
        JSONObject json = new JSONObject();
        json.put("ret", 0);
        json.put("msg", "成功获取该城市！");
        json.put("city", regionRep.findByProvCityName(provName,cityName));
        return json;
    }

    @ApiOperation(value = "获取该省份为id的所有的城市/或者 获取该城市为id的所有区/县", httpMethod = "GET")
    @GetMapping("/Cities/Provs/{id}")
    @ApiImplicitParam(name="id",value="省份的id",dataType = "Integer",dataTypeClass = Integer.class, required = true)
    public JSONObject getAllCities(@PathVariable Integer id) {
        JSONObject json = new JSONObject();
        json.put("ret", 0);
        json.put("msg", "成功获取城市信息！");
        json.put("cities", regionRep.findAllCitiesInProvOrderByPinyin(id));
        return json;
    }
}
package edu.zhku.uecp.controller.courses;

import edu.zhku.uecp.model.Course_Type;
import edu.zhku.uecp.model.Course_TypeRepository;
import edu.zhku.uecp.service.courses.C_TypeService;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Map;

@Api(tags = "与 课程类型 相关的API接口")
@RestController
@RequestMapping("/course_type")
public class CourseTypeController {

    @Autowired
    private C_TypeService c_typeService;


    @ApiOperation(value = "添加课程类型")
    @PostMapping("/add")
    public JSONObject addType(Course_Type c_type){
        JSONObject json = new JSONObject();
        int tid = c_typeService.addCourseType(c_type);
        if(c_typeService.isExit(tid)){
            json.put("ret", 0);
            json.put("msg", "添加成功！课程类型id为： "+tid);
        }else{
            json.put("ret", 1);
            json.put("msg", "添加失败！");
        }
        return json;
    }


    @ApiOperation(value = "根据id删除课程类型")
    @DeleteMapping(value = "/del/{id}")
    @ApiImplicitParam(name="id",value="要删除的课程类型id",dataType = "Integer",dataTypeClass = Integer.class, required = true)
    public JSONObject deleteCourseType(@PathVariable Integer id){
        JSONObject json = new JSONObject();
        if(id != null && c_typeService.isExit(id)){
            c_typeService.deleteCourseType(id);
            if(!c_typeService.isExit(id)){
                json.put("ret", 0);
                json.put("msg", "删除成功");
            }else{
                json.put("ret", 1);
                json.put("msg", "删除失败");
            }
            return json;
        }
        json.put("ret", 2);
        json.put("msg", "找不到该课程类型！");
        return json;
    }


    @ApiOperation(value = "通过id修改课程类型")
    @PutMapping("/update/{id}")
    @ApiImplicitParam(name="id",value="要更新的课程类型id", dataType = "Integer",dataTypeClass = Integer.class,required = true)
    public JSONObject updateCourseType(@PathVariable Integer id, Course_Type c_type){
        JSONObject json = new JSONObject();
        if(id != null && c_typeService.isExit(id)) {
            String name = c_typeService.updateCourseType(id,c_type);
            json.put("ret", 0);
            json.put("msg", "更新课程类型 "+name+" 成功！");
        }else{
            json.put("ret", 1);
            json.put("msg", "找不到该课程类型！");
        }
        return json;
    }


    @ApiOperation(value = "获取所有的课程类型")
    @GetMapping(value = "/get")
    public JSONObject getAllCourseTypes(){
        JSONObject json = new JSONObject();
        Map<String, Object> courseTypes = c_typeService.getAllType();
        json.put("ret", 0);
        json.put("msg", "成功获取所有课程类型！");
        json.put("courseTypes", courseTypes);
        return json;
    }


    @ApiOperation(value = "分页 获取所有的课程类型")
    @GetMapping(value = "/get/{pageNum}/{pageSize}")
    @ApiImplicitParams({
            @ApiImplicitParam(name="pageNum",value="页码",dataType = "Integer",dataTypeClass = Integer.class, required = true),
            @ApiImplicitParam(name="pageSize",value="一页的个数",dataType = "Integer",dataTypeClass = Integer.class, required = true)
    })
    public JSONObject getAllCourseTypesByPage(@PathVariable Integer pageNum, @PathVariable Integer pageSize){
        JSONObject json = new JSONObject();
        Map<String, Object> courseTypes = c_typeService.getAllTypeByPage(pageNum-1, pageSize);
        json.put("ret", 0);
        json.put("msg", "成功获取课程类型！");
        json.put("courseTypes", courseTypes);
        return json;
    }


    @ApiOperation(value = "根据 关键词 获取课程类型")
    @GetMapping(value = "/get/{name}")
    @ApiImplicitParam(name="name",value="关键词", dataType = "String",dataTypeClass = String.class,required = true)
    public JSONObject getTypeByName(String name){
        JSONObject json = new JSONObject();
        Map<String, Object> courseType = c_typeService.getTypesByName(name);
        json.put("ret", 0);
        json.put("msg", "成功获取所有课程类型！");
        json.put("courseType", courseType);
        return json;
    }
}
package edu.zhku.uecp.controller.courses;

import edu.zhku.uecp.model.Course;
import edu.zhku.uecp.model.Course_VideoRepository;
import edu.zhku.uecp.service.courses.C_VideoService;
import edu.zhku.uecp.model.Course_Video;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Map;

@Api(tags = "与 课程视频管理 相关的API接口")
@RestController
@RequestMapping("/course_video")
public class VideoController {

    @Autowired
    private C_VideoService c_videoService;


    @ApiOperation(value = "添加单个课程视频的信息")
    @PostMapping("/add")
    public JSONObject addOneVideoMsg(Course_Video c_video){
        JSONObject json = new JSONObject();
        String c_vName = c_videoService.addVideo(c_video);
        if(c_vName != null){
            json.put("ret", 0);
            json.put("msg", "添加 "+c_vName+" 成功！");
        }else{
            json.put("ret", 1);
            json.put("msg", "添加视频失败！");
        }
        return json;
    }


    @ApiOperation(value = "通过id删除课程视频信息")
    @DeleteMapping("/delete/{id}")
    @ApiImplicitParam(name="id",value="要删除的视频id", dataType = "Integer",dataTypeClass = Integer.class, required = true)
    public JSONObject deleteVideoById(@PathVariable Integer id){
        JSONObject json=new JSONObject();
        if(id != null && c_videoService.isExist(id)) {
            c_videoService.deleteVideo(id);
            if(!c_videoService.isExist(id)){
                json.put("ret", 0);
                json.put("msg", "成功删除Id为 "+id+" 的视频！");
            }else{
                json.put("ret", 2);
                json.put("msg", "删除失败！");
            }
        }else {
            json.put("ret", 1);
            json.put("msg", "找不到该视频！");
        }
        return json;
    }


    @ApiOperation(value = "通过id修改课程视频信息")
    @PutMapping("/update/{id}")
    @ApiImplicitParam(name="id",value="要更新的视频id", dataType = "Integer",dataTypeClass = Integer.class,required = true)
    public JSONObject updateVideoMsgById(@PathVariable Integer id,Course_Video c_video){
        JSONObject json = new JSONObject();
        if(id != null && c_videoService.isExist(id)) {
            String c_vName = c_videoService.updateVideo(id,c_video);
            json.put("ret", 0);
            json.put("msg", "更新视频 "+c_vName+" 成功！");
        }else{
            json.put("ret", 1);
            json.put("msg", "找不到该视频！");
        }
        return json;
    }


    @ApiOperation(value = "根据 课程id 获取所有的 课程视频信息")
    @GetMapping(value = "/get/{id}")
    @ApiImplicitParam(name="id",value="课程id", dataType = "Integer",dataTypeClass = Integer.class,required = true)
    public JSONObject getAllVideosByCourseId(@PathVariable Integer id){
        JSONObject json = new JSONObject();
        Map<String, Object> c_videos = c_videoService.findAllByCourseId(id);
        json.put("ret", 0);
        json.put("msg", "成功获取该课程视频！");
        json.put("course_videos", c_videos);
        return json;
    }
}
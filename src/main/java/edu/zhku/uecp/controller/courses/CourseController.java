package edu.zhku.uecp.controller.courses;

import com.alibaba.fastjson.JSONObject;
import edu.zhku.uecp.model.CourseRepository;
import edu.zhku.uecp.model.User;
import edu.zhku.uecp.model.Course;
import edu.zhku.uecp.service.courses.CourseService;
import edu.zhku.uecp.service.TokenService;
import edu.zhku.uecp.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Map;
import java.util.List;

@Api(tags = "与 课程管理 相关的API接口")
@RestController
@RequestMapping("/course")
public class CourseController {

    @Autowired
    private UserService userService;
    @Autowired
    private TokenService tokenService;
    @Autowired
    private CourseService courseService;


    @ApiOperation(value = "不分页 获取所有的课程信息")
    @GetMapping(value = "/get")
    public JSONObject getAllCoursesNoPage(){
        JSONObject json = new JSONObject();
        Map<String, Object> courses = courseService.findAllCourseNoPage();
        json.put("ret", 0);
        json.put("msg", "成功获取所有课程信息！");
        json.put("courses", courses);
        return json;
    }


    @ApiOperation(value = "分页 获取所有的课程信息")
    @GetMapping(value = "/get/{pageNum}/{pageSize}")
    @ApiImplicitParams({
            @ApiImplicitParam(name="pageNum",value="页码",dataType = "Integer",dataTypeClass = Integer.class, required = true),
            @ApiImplicitParam(name="pageSize",value="一页的个数",dataType = "Integer",dataTypeClass = Integer.class, required = true)
    })
    public JSONObject getAllCoursesByPage(@PathVariable Integer pageNum, @PathVariable Integer pageSize){
        JSONObject json = new JSONObject();
        if(pageNum != null && pageSize != null){
            Map<String, Object> courses = courseService.findAllCourseByPage(pageNum-1,pageSize);
            json.put("ret", 0);
            json.put("msg", "成功获取所有课程信息！");
            json.put("courses", courses);
        }else{
            json.put("ret", 1);
            json.put("msg", "缺少页码或个数！");
        }
        return json;
    }


    @ApiOperation(value = "添加 单个 课程信息")
    @PostMapping("/add")
    public JSONObject addOneCourseMsg(Course course){
        JSONObject json = new JSONObject();
        int cid = courseService.saveOneCourseMsg(course);
        if(courseService.isExist(cid)){
            json.put("ret", 0);
            json.put("msg", "添加成功！课程id为 "+cid);
        }else{
            json.put("ret", 1);
            json.put("msg", "添加课程失败！");
        }
        return json;
    }


    @ApiOperation(value = "通过id 修改 课程信息")
    @PutMapping("/update/{id}")
    @ApiImplicitParam(name="id",value="要更新的课程id", dataType = "Integer",dataTypeClass = Integer.class,required = true)
    public JSONObject updateCourseMsgById(@PathVariable Integer id,Course course){
        JSONObject json = new JSONObject();
        String cName;
        if(id != null && courseService.isExist(id)) {
            cName = courseService.updateCourseMsg(id,course);
            json.put("ret", 0);
            json.put("msg", "更新课程"+ cName +"成功！");
        }else{
            json.put("ret", 1);
            json.put("msg", "找不到该课程！");
        }
        return json;
    }


    @ApiOperation(value = "分页 通过 课程名称或老师名称 搜索课程信息")
    @GetMapping("search/{name}/{pageNum}/{pageSize}")
    @ApiImplicitParams({
            @ApiImplicitParam(name="name",value="输入查询的课程名称或老师名称", dataType = "String",dataTypeClass = String.class,required = true),
            @ApiImplicitParam(name="pageNum",value="页码",dataType = "Integer",dataTypeClass = Integer.class, required = true),
            @ApiImplicitParam(name="pageSize",value="一页的个数",dataType = "Integer",dataTypeClass = Integer.class, required = true)
    })
    public JSONObject searchCourseMsgByName(@RequestParam String name, @PathVariable Integer pageNum, @PathVariable Integer pageSize){
        JSONObject json = new JSONObject();
        if(name != null){
            if(pageNum != null && pageSize != null){
                Map<String, Object> courses = courseService.searchCourseByName(name,pageNum-1,pageSize);
                json.put("ret", 0);
                json.put("msg", "成功获取课程信息！");
                json.put("courses", courses);
            }else {
                json.put("ret", 1);
                json.put("msg", "缺少页数或者一页的个数！");
            }
        }else{
            json.put("ret", 2);
            json.put("msg", "输入查询的名字为空！");
        }
        return json;
    }


    @ApiOperation(value = "通过id 删除 课程信息")
    @DeleteMapping("/delete/{id}")
    @ApiImplicitParam(name="id",value="要删除的课程id", dataType = "Integer",dataTypeClass = Integer.class, required = true)
    public JSONObject deleteCourseById(@PathVariable Integer id){
        JSONObject json=new JSONObject();
        if(id != null && courseService.isExist(id)) {
            if(courseService.deleteCourse(id)){
                json.put("ret", 0);
                json.put("msg", "成功删除Id为" + id + "的课程！");
            }else{
                json.put("ret", 1);
                json.put("msg", "删除失败！");
            }
        }else {
            json.put("ret", 2);
            json.put("msg", "找不到该课程！");
        }
        return json;
    }


    @ApiOperation(value = "获取所有的课程等级")
    @GetMapping(value = "/getLevels")
    public JSONObject getAllCourseLevels(){
        JSONObject json = new JSONObject();
        List<String> courseLevels = courseService.getAllCourseLevel();
        json.put("ret", 0);
        json.put("msg", "成功获取所有课程等级！");
        json.put("courseLevels", courseLevels);
        return json;
    }


    @ApiOperation(value = "分页 获取根据 评分 升序排序 的课程")
    @GetMapping(value = "/sortByScore/{pageNum}/{pageSize}")
    @ApiImplicitParams({
            @ApiImplicitParam(name="pageNum",value="页码",dataType = "Integer",dataTypeClass = Integer.class, required = true),
            @ApiImplicitParam(name="pageSize",value="一页的个数",dataType = "Integer",dataTypeClass = Integer.class, required = true)
    })
    public JSONObject getSortAllCourse(@PathVariable Integer pageNum, @PathVariable Integer pageSize){
        JSONObject json = new JSONObject();
        if(pageNum != null && pageSize != null){
            Map<String, Object> courses = courseService.searchSortAllCourse(pageNum-1,pageSize);
            json.put("ret", 0);
            json.put("msg", "成功获取所有课程信息！");
            json.put("courses", courses);
        }else {
            json.put("ret", 1);
            json.put("msg", "缺少页数或者一页的个数！");
        }
        return json;
    }


    @ApiOperation(value = "根据 课程id 查询 课程信息（无需权限）")
    @GetMapping("/search/{id}")
    @ApiImplicitParam(name="id",value="要查询的课程id", dataType = "Integer",dataTypeClass = Integer.class, required = true)
    public JSONObject searchCourseById(@PathVariable Integer id){
        JSONObject json = new JSONObject();
        if(id !=null){
            if(courseService.isExist(id)){
                Map<String, Object> courses = courseService.searchCourseById(id);
                json.put("ret", 0);
                json.put("msg", "成功获取课程！");
                json.put("courses", courses);
            }else{
                json.put("ret", 1);
                json.put("msg", "找不到id为"+ id +"的课程！");
            }
        }else{
            json.put("ret", 2);
            json.put("msg", "id为空！");
        }
        return json;
    }


    @ApiOperation(value = "根据 课程id 查询 课程信息（需要权限）",notes = "暂时设置只有系统管理拥有权限")
    @GetMapping("/authSearch/{id}")
    @ApiImplicitParams({
            @ApiImplicitParam(name="userId",value="用户Id",  dataType = "Integer",dataTypeClass = Integer.class,required = true),
            @ApiImplicitParam(name="Authorization",value="用户校验码token",  dataType = "String",dataTypeClass = String.class,required = true),
            @ApiImplicitParam(name="id",value="要查询的课程id", dataType = "Integer",dataTypeClass = Integer.class, required = true)
    })
    public JSONObject authSearchCourseById(@RequestHeader("userId") Integer usrid, @RequestHeader("Authorization") String strToken,
                                           @PathVariable Integer id){
        JSONObject json = new JSONObject();
        User usr = userService.getUserById(usrid);
        if(strToken != null && usr != null) {
            if (tokenService.verify(strToken, usr.getId(), usr.getTokenSecretKey()) != 0) {
                json.put("ret", 1);
                json.put("msg", "token签名校验失败！");
                return json;
            }
            if (id != null) {
                if(usr.getRole() == 128){//系统管理员
                    Map<String, Object> courses = courseService.searchCourseById(id);
                    json.put("ret", 0);
                    json.put("msg", "成功获取课程信息！");
                    json.put("courses", courses);
                    return json;
                }
                json.put("ret", 4);
                json.put("msg", "不允许该操作！");
            }else{
                json.put("ret", 3);
                json.put("msg", "课程id为空！");
            }
            return json;
        }
        json.put("ret", 2);
        json.put("msg", "缺少用户ID或token！");
        return json;
    }


    @ApiOperation(value = "根据 机构id 查询 课程信息")
    @GetMapping("/search/{org_id}")
    @ApiImplicitParam(name="org_id",value="机构id", dataType = "String",dataTypeClass = String.class, required = true)
    public JSONObject searchCourseById(String org_id){
        JSONObject json = new JSONObject();
        if(org_id != null){
            Map<String, Object> courses = courseService.searchCourseByOrgId(org_id);
            if(courses != null){
                json.put("ret", 0);
                json.put("msg", "成功获取课程！");
                json.put("courses", courses);
            }else{
                json.put("ret", 1);
                json.put("msg", "找不到课程！");
            }
        }else{
            json.put("ret", 2);
            json.put("msg", "机构id为空！");
        }
        return json;
    }
}
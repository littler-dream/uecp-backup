package edu.zhku.uecp.controller;

import com.alibaba.fastjson.JSONObject;
import edu.zhku.uecp.service.MinioService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Api(tags = "与minio有关的接口")
@RestController
public class MinioController {
    @Autowired
    private MinioService minioService;


    /**
     * 上传
     * @param file
     * @return
     */
    @ApiOperation(value = "上传文件")
    @PostMapping("/minio/upload")
    public Map<String,Object> upload(@ApiParam(value = "文件",required = true) @RequestPart(name = "file") MultipartFile file) {
        JSONObject json = new JSONObject();
        String url;
        // 判断上传文件是否为空
        if (file.isEmpty()) {
            json.put("ret", 1);
            json.put("msg", "上传文件不能为空");
            return json;
        }
        try {
            url = minioService.uploadFile(file);
        } catch (Exception e) {
            e.printStackTrace();
            json.put("ret", 2);
            json.put("msg", "上传失败");
            return json;
        }
        json.put("ret", 0);
        json.put("msg", "上传成功");
        json.put("url", url);
        return json;
    }


}


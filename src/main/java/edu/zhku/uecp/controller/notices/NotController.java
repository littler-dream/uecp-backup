package edu.zhku.uecp.controller.notices;

import com.alibaba.fastjson.JSONObject;
import edu.zhku.uecp.model.Enterprise;
import edu.zhku.uecp.model.Notices;
import edu.zhku.uecp.model.User;
import edu.zhku.uecp.service.NotService;
import edu.zhku.uecp.service.TokenService;
import edu.zhku.uecp.service.UserService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

@Api(tags = "与公告管理相关的API接口")  //(swagger)API接口类文档
@RestController
@RequestMapping("/notices/nots")
public class NotController {
    @Autowired
    private NotService notService;
    @Autowired
    private UserService userService;
    @Autowired
    private TokenService tokenService;

    @ApiOperation(value = "获取公告通过id")
    @GetMapping(value = "/{id}")
    @ApiImplicitParam(name = "id", value = "公告id", dataType = "Integer", dataTypeClass = Integer.class, required = true)
    public JSONObject getAllNots(@PathVariable Integer id) {
        JSONObject json = new JSONObject();
        if (id != null) {
            if (notService.isExist(id)) {
                Notices not;
                not = notService.findById(id);
                json.put("ret", 0);
                json.put("msg", "成功获取得到所有公告信息！");
                json.put("not", not);
                return json;
            } else {
                json.put("ret", 2);
                json.put("msg", "id为" + id + "的公告不存在！");
                return json;
            }
        } else {
            json.put("ret", 1);
            json.put("msg", "缺少公告id！");
            return json;
        }
    }

    @ApiOperation(value = "获取所有的公告信息，分页, 根据优先级升序", notes = "页数从1开始")
    @GetMapping(value = "/{pageNum}/{pageSize}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页码", dataType = "Integer", dataTypeClass = Integer.class, required = true),
            @ApiImplicitParam(name = "pageSize", value = "一页的个数", dataType = "Integer", dataTypeClass = Integer.class, required = true)
    })
    public JSONObject getAllNots(@PathVariable Integer pageNum, @PathVariable Integer pageSize) {
        JSONObject json = new JSONObject();
        if (pageNum != null && pageSize != null) {
            Map<String, Object> orgs = notService.findAllNots(pageNum - 1, pageSize);
            json.put("ret", 0);
            json.put("msg", "成功获取到所有公告信息！");
            json.put("orgs", orgs);
            return json;
        } else {
            json.put("ret", 1);
            json.put("msg", "缺少页数或者一页的个数！");
            return json;
        }

    }

    @ApiOperation(value = "获取所有的公告信息, 不分页")
    @GetMapping(value = "/")
    public JSONObject getAllNotsNoPage() {
        JSONObject json = new JSONObject();
        Map<String, Object> orgs = notService.findAllNotsNoPage();
        json.put("ret", 0);
        json.put("msg", "成功获取得到所有公告信息！");
        json.put("orgs", orgs);
        return json;
    }

    @ApiOperation(value = "通过Id更新公告 （只能系统管理员操作）")
    @PutMapping("/{id}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "用户校验码token", dataType = "String", dataTypeClass = String.class, required = true),
            @ApiImplicitParam(name = "userId", value = "用户Id", dataType = "Integer", dataTypeClass = Integer.class, required = true),
            @ApiImplicitParam(name = "id", value = "要更新的公告id", dataType = "Integer", dataTypeClass = Integer.class, required = true),
    })

    public JSONObject upNotById(@RequestHeader("Authorization") String strToken, @RequestHeader("userId") Integer usrid,
                                @PathVariable Integer id, @RequestBody Notices not) {
        JSONObject json = new JSONObject();
        String befpicture;
        User usr = userService.getVeriSysAdm(usrid);
        if ((usr != null && tokenService.verify(strToken, usrid, usr.getTokenSecretKey()) == 0)) {
            if (id != null && notService.isExist(id)) {
                befpicture = notService.upNot(id, not);
                json.put("ret", 0);
                json.put("msg", "更改公告成功！");

            } else {
                json.put("ret", 1);
                json.put("msg", "找不到该公告！");
                return json;
            }
        } else {
            json.put("ret", 2);
            json.put("msg", "不允许该操作！");
        }
        return json;
    }

    @ApiOperation("添加单个公告 (系统管理员)")
    @PostMapping("/")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "用户校验码token", dataType = "String", dataTypeClass = String.class, required = true),
            @ApiImplicitParam(name = "userId", value = "用户Id", dataType = "Integer", dataTypeClass = Integer.class, required = true)
    })
    public JSONObject addOneNot(@RequestHeader("Authorization") String strToken, @RequestHeader("userId") Integer usrid,
                                @RequestBody Notices not) {
        JSONObject json = new JSONObject();
        User usr = userService.getVeriSysAdm(usrid);//系统管理员
        if ((usr != null && tokenService.verify(strToken, usrid, usr.getTokenSecretKey()) == 0)) {
            if (not.getPicture() != null) {
                notService.addNot(not);
                json.put("ret", 0);
                json.put("msg", "添加成功！");
            } else {
                json.put("ret", 1);
                json.put("msg", "公告url为空！");
                return json;
            }
        } else {
            json.put("ret", 2);
            json.put("msg", "不允许该操作！");
        }
        return json;
    }

    @ApiOperation("通过Id删除公告 (系统管理员)")
    @DeleteMapping("/{id}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "用户校验码token", dataType = "String", dataTypeClass = String.class, required = true),
            @ApiImplicitParam(name = "userId", value = "用户Id", dataType = "Integer", dataTypeClass = Integer.class, required = true),
            @ApiImplicitParam(name = "id", value = "要删除的公告id", dataType = "Integer", dataTypeClass = Integer.class, required = true),
    })
    public JSONObject delNotById(@RequestHeader("Authorization") String strToken, @RequestHeader("userId") Integer usrid,
                                 @PathVariable Integer id) {
        JSONObject json = new JSONObject();
        User usr = userService.getVeriSysAdm(usrid);//系统管理员
        if ((usr != null && tokenService.verify(strToken, usrid, usr.getTokenSecretKey()) == 0)) {
            if (id != null && notService.isExist(id)) {
                notService.delNot(id);
                json.put("ret", 0);
                json.put("msg", "成功删除Id为" + id + "的公告！");
            } else {
                json.put("ret", 1);
                json.put("msg", "找不到该公告！");
                return json;
            }
        } else {
            json.put("ret", 2);
            json.put("msg", "不允许该操作！");
        }
        return json;
    }

    @ApiOperation(value = "通过关键字模糊查找公告", notes = "返回一页pageSize个记录，页数从1开始")
    @GetMapping(value = "/byKeyword/{pageNum}/{pageSize}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "用户校验码token", dataType = "String", dataTypeClass = String.class, required = true),
            @ApiImplicitParam(name = "userId", value = "用户Id", dataType = "Integer", dataTypeClass = Integer.class, required = true),
            @ApiImplicitParam(name = "keyword", value = "输入查询的关键字", dataType = "String", dataTypeClass = String.class, required = true),
            @ApiImplicitParam(name = "pageNum", value = "页码", dataType = "Integer", dataTypeClass = Integer.class, required = true),
            @ApiImplicitParam(name = "pageSize", value = "一页的个数", dataType = "Integer", dataTypeClass = Integer.class, required = true)
    })
    public JSONObject searchNotByKeyword(@RequestHeader("Authorization") String strToken, @RequestHeader("userId") Integer usrid,
                                         @RequestParam String keyword, @PathVariable Integer pageNum,
                                         @PathVariable Integer pageSize) {
        JSONObject json = new JSONObject();//用于输出res data
        User usr = userService.getVeriSysAdm(usrid);//系统管理员
        if (strToken != null && usr != null) {
            if (tokenService.verify(strToken, usr.getId(), usr.getTokenSecretKey()) != 0) {
                json.put("ret", 1);
                json.put("msg", "token签名校验失败！");
                return json;
            }
            if (keyword != null) {
                if (pageNum != null && pageSize != null) {
                    Map<String, Object> orgs = notService.searchNotBykeyword(keyword, pageNum - 1, pageSize);
                    json.put("ret", 0);
                    json.put("msg", "成功获取得到公告信息！");
                    json.put("orgs", orgs);
                    return json;
                } else {
                    json.put("ret", 4);
                    json.put("msg", "缺少页数或者一页的个数！");
                }
            } else {
                json.put("ret", 3);
                json.put("msg", "输入查询的关键字为空！");
                return json;
            }
        }
        json.put("ret", 2);
        json.put("msg", "缺少用户ID或token！");
        return json;
    }

    @ApiOperation(value = "通过关键字模糊查找公告，不分页版")
    @GetMapping(value = "/byKeyword")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "用户校验码token", dataType = "String", dataTypeClass = String.class, required = true),
            @ApiImplicitParam(name = "userId", value = "用户Id", dataType = "Integer", dataTypeClass = Integer.class, required = true),
            @ApiImplicitParam(name = "keyword", value = "输入查询的关键字", dataType = "String", dataTypeClass = String.class, required = true),
    })
    public JSONObject searchNotByKeywordNoPage(@RequestHeader("Authorization") String strToken,
                                               @RequestHeader("userId") Integer usrid,
                                               @RequestParam String keyword) {
        JSONObject json = new JSONObject();//用于输出res data
        User usr = userService.getVeriSysAdm(usrid);//系统管理员
        if (strToken != null && usr != null) {
            if (tokenService.verify(strToken, usr.getId(), usr.getTokenSecretKey()) != 0) {
                json.put("ret", 1);
                json.put("msg", "token签名校验失败！");
                return json;
            }
            if (keyword != null) {
                Map<String, Object> orgs = notService.searchNotBykeywordNoPage(keyword);
                json.put("ret", 0);
                json.put("msg", "成功获取得到公告信息！");
                json.put("orgs", orgs);
                return json;
            } else {
                json.put("ret", 3);
                json.put("msg", "输入查询的关键字为空！");
                return json;
            }
        }
        json.put("ret", 2);
        json.put("msg", "缺少用户ID或token！");
        return json;

    }
}
